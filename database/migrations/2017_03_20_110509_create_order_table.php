<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order', function(Blueprint $table)
		{
			$table->integer('OrderID')->primary();
			$table->dateTime('Date/Time_Created');
			$table->integer('OrderStatus');
			$table->integer('StudentID');
			$table->integer('TeacherID');
			$table->integer('OrderType');
			$table->integer('Price');
			$table->string('Description', 5000);
			$table->string('Duration', 500);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order');
	}

}
