<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderrequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderrequest', function(Blueprint $table)
		{
			$table->integer('RequestID')->primary();
			$table->dateTime('Date/Time_Created');
			$table->integer('StudentID');
			$table->integer('TeacherID');
			$table->integer('OrderType');
			$table->integer('RequestPrice');
			$table->string('RequestDescription', 5000);
			$table->string('RequestDuration', 500);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderrequest');
	}

}
