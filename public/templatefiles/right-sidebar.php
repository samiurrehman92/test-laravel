<div class="panel panel-default right-sidebar-widget">
	<div class="panel-heading">
		Other Popular Teachers or Students of this Course
	</div>
	<div class="panel-body">
		<ul>
			<li>Teacher Abdullah</li>
			<li>Teacher Shamim</li>
			<li>Teacher Imran</li>
			<li>Student Hamza</li>
			<li>Student Qasim</li>
			<li>Student Fatima</li>
		</ul>
	</div>
</div>

<div class="panel panel-default right-sidebar-widget">
	<div class="panel-heading">
		Other Courses That Might Interest You!
	</div>
	<div class="panel-body">
		<ul>
			<li>Physics</li>
			<li>Chemistry</li>
			<li>Maths</li>
			<li>Science</li>
			<li>English</li>
			<li>Urdu</li>
		</ul>
	</div>
</div>