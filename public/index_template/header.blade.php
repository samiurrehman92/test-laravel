    <div class="header">    
      <nav class="navbar navbar-default main-navigation" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand logo" href="/" style="width:auto;">
				<img src="images/logo.png" alt="Smart-Ed"> <h1 >Smart-Ed</h1>
			</a>
          </div>
          <!-- brand and toggle menu for mobile End -->

          <!-- Navbar Start -->
          <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo url('/login') ?>  "><i class="lnr lnr-enter"></i> Login</a></li>
              <li><a href=" <?php echo url('/signup') ?> "><i class="lnr lnr-user"></i> Signup</a></li>
            </ul>
          </div>
          <!-- Navbar End -->
        </div>
      </nav>
    </div>
