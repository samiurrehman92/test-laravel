<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//HOMEPAGE AND LOGIN SIGN UP ROUTES
///////////////////////////////////

Route::post("/materialorder", 'orderRequestController@matrials');


Route::get('/dashboard/uploadDocs', function() {
	return view('dashboard/upload-doc');
});
Route::post('/dashboard/uploadDocs', 'teacherController@uploadDocs');

Route::get('/dashboard/Docs/nomore/{file_id}', 'teacherController@deletef');

Route::get('/dashboard/myfiles', 'usersController@myfiles');

Route::get('/dashboard/all-files', 'teacherController@userfiles');


Route::get('/', 'IndexController@index');

Route::get('login', 'Auth\LoginController@show');

Route::get('signup', function() {
	if(Auth::check())
		return redirect('/dashboard')->with('message','Welcome back!');
	return view('signup');
});

Route::post('/signup', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login' );
Route::get('/logout', 'Auth\LoginController@logout');

Route::post('/social_login/', 'Auth\LoginController@social_login');
Route::get('/social_login/twitter', 'Auth\LoginController@show_twitter_login');
Route::get('/social_login/twitter_callback', 'Auth\LoginController@twitter_callback');

Route::get('/profile/{user_id}', 'usersController@showProfile');


//DASHBOARD ROUTES
///////////////////////

Route::get('/dashboard', function() {
	return view('dashboard');
});

Route::get('/dashboard/chat/{user_id}', 'ChatController@index');
Route::get('/dashboard/customer-support', 'ChatController@admin');
Route::post('/chat_send', 'ChatController@sendMsg');
Route::get('/chat_get', 'ChatController@getMsg');


Route::get('/dashboard/edit-profile', 'editProfileController@index');
Route::post('/updateuser',  'editProfileController@updateuser' );
Route::post('/postPic', function () {
    $file = request()->file('dp');
    $file->storeas('/public/avatars',Auth()->id().'dp.jpg');
    return redirect('/dashboard/edit-profile');
});

Route::post('/dashboard/update_password',  'editProfileController@update_password' );


//SEARCH FUNCTIONALITIES
//////////////////////////
Route::get('/dashboard/search-teachers', 'searchUsersController@allTeachers');
Route::post('/dashboard/search-teachers', 'searchUsersController@searchTeachers');

Route::get('/dashboard/search-students', 'searchUsersController@allStudents');

//TRANSACTIONS FUNCTIONALITIES
Route::get('/dashboard/view-transactions', 'transactionController@view_all');
Route::get('/dashboard/view-transactions/withdrawal', 'transactionController@withdrawal');


//ORDER FUNCTIONALITIES
/////////////////////////
Route::get("/dashboard/create-order-request/{user_id}", 'orderRequestController@index');
Route::post("/dashboard/create-order-request/store", 'orderRequestController@store');

Route::get("/dashboard/respond-to-order-request/{order_id}", 'orderRequestController@create');
Route::post("/dashboard/respond-to-order-request/Respond", 'orderRequestController@Respond');

Route::get("/dashboard/order/{order_id}", 'orderRequestController@show');
Route::post("/dashboard/order/update_order_time", 'orderRequestController@update_remaining_time');

Route::get("/dashboard/completed-order/{order_id}", 'orderRequestController@completeOrder');
Route::post("/dashboard/completed-order/AcceptDelivery", 'orderRequestController@AcceptDelivery');
Route::post("/dashboard/completed-order/RequestRevision", 'orderRequestController@RequestRevision');


Route::get("/dashboard/create-order/{order_id}", 'orderRequestController@createOrder');
Route::post("/dashboard/create-order/checkout", 'orderRequestController@checkout');


//ADMIN FUNCTIONALITIES
/////////////////////////
Route::get('/dashboard/all-users', 'usersController@view_all');
Route::get('/dashboard/add-cards', 'PaymentController@addcard');
Route::get('/dashboard/all-orders', 'orderRequestController@view_all');
Route::get("/dashboard/suspend_order/{order_id}", 'orderRequestController@suspend');
Route::get("/dashboard/activate_order/{order_id}", 'orderRequestController@activate');
Route::get("/dashboard/suspend_user/{user_id}", 'usersController@suspend');
Route::get("/dashboard/activate_user/{user_id}", 'usersController@activate');
Route::get("/dashboard/delete_user/{user_id}", 'usersController@deleteuser');
Route::post("/dashboard/addcards", 'PaymentController@generate');
Route::get('/dashboard/user-settings', "usersController@showSettings");

Route::post("deactivate", 'usersController@deactivate');


//CARD LOAD
/////////////////////////////////////////////
Route::post("/dashboard/load", 'PaymentController@index');
Route::get('/dashboard/card-load', function () {
    return view('/dashboard/card-load');
});

//Auth::routes();

// Registration Routes...
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes... 
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index');

Route::get('/dashboard', 'HomeController@index');