<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class smartcards extends Model
{
	protected $table = 'smartcards';

	public $timestamps = false;
    //
}
