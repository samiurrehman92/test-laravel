<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderrequest extends Model
{
	public $table = "orderrequest";
    protected $fillable =['User_ID','teacher','ordertype','course','description','duration','price'];
}
