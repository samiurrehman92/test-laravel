<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


use App\User;
use App\teacher;
use Auth;

class searchUsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		date_default_timezone_set("Asia/Karachi");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('dashboard/search-teachers');
    }
	
	public function allTeachers()
	{
		$teachers = DB::table('users')
            ->select('id','name')
            ->where('teacher', '=', '1')
            ->get();
			
		foreach($teachers as $teacher)
		{
			//query the teachers database and set other parameters here
		}
		return view('dashboard/search-teachers')->with('users',$teachers);
	}

	public function searchTeachers()
	{
		/*$teachers = DB::table('users')
            ->select('id','name')
            ->where('teacher', '=', '1')
            ->where('name', 'LIKE', '%'.$_REQUEST["name"].'%')
            ->get();*/
            $charity=0;
            $olevel=0;
            $matric=0;
            $alevel=0;
            $country="";
            $city="";

        $teachers_query=    DB::table('users')
            ->select('id','name')
        		->join('teacher', function ($join) {
            		$join->on('users.id', '=', 'teacher.UserId')
                 	->where('name', 'LIKE', '%'.$_REQUEST["name"].'%');
        			});


        			$teachers_query->where('rating', '>=', $_REQUEST["rating"]);
        			$teachers_query->where('experience', '>=', $_REQUEST["experience"]);

        				
        			 if ($_REQUEST["country"]!="0") 
        			 {
        			 	$teachers_query->where('country', 'LIKE', '%'.$_REQUEST["country"].'%');
        			 }
        			 if ($_REQUEST["city"]!="0") {
        			 	$teachers_query->where('city', 'LIKE', '%'.$_REQUEST["city"].'%');
        			 }
        			
                 	
        			
        			if(isset($_REQUEST["Matric"]))
                 	{
                 		
                 		$teachers_query->orwhere('standards', 'LIKE', '%Matric%');
                 		$matric=1;
                 	}
                 	if(isset($_REQUEST["Olevel"]))
                 	{
                 		$olevel=1;
                 		$teachers_query->orwhere('standards', 'LIKE', '%Olevel%');
                 	}
                 	if(isset($_REQUEST["Alevel"]))
                 	{
                 		
                 		$teachers_query->orwhere('standards', 'LIKE', '%Alevel%');
                 		$alevel=1;
                 	}
                 	if(isset($_REQUEST["charity"]))
                 	{
                 		
                 		$teachers_query->where('TeachForCharity', '=', '1');
                 		$charity=1;
                 	}
      $teachers = $teachers_query ->get();
      $data = array('country' => $_REQUEST["country"], 
      	'charity'=>$charity,
      	'matric'=>$matric,
      	'olevel'=>$olevel,
      	'alevel'=>$alevel,
      	'name'=>$_REQUEST["name"],
      	'country'=>$_REQUEST["country"],
      	'city'=>$_REQUEST["city"]



      	);
      

		/*foreach($teachers as $teacher)
		{
			//query the teachers database and set other parameters here
		}*/
		return view('dashboard/search-teachers')->with('users',$teachers)->with('data',$data);
	}
	
	public function allStudents()
	{
		$students = DB::table('users')
            ->select('id','name')
            ->where('teacher', '=', '0')
            ->get();
			
		foreach($students as $student)
		{
			//query the students database and set other parameters here
		}
		return view('dashboard/search-students')->with('users',$students);
	}	
}
