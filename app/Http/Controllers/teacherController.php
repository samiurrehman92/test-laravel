<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\teacher;
//use App\file;


class teacherController extends Controller
{

	public function uploadDocs(Request $request)
	{
		//echo "helo!";

		$data = $request->all();

		if(!isset($data['privacy']))
				$data['privacy']=0;

		if(!isset($data['type']))
			$data['type']=0;


		$fileName= request()->file('doc')->getClientOriginalName();

		$file = request()->file('doc');
    $file->storeas('public/teacher_docs',Auth()->id().'_'.$fileName);

    $name=Auth()->id().'_'.$fileName;

    DB::table('files')->insert(
    ['Uid' => Auth()->id(), 'type'=>$data['type'], 'privacy' => $data['privacy'],'title' => $data['title'],'subject' => $data['course'],'name'=>$name]
	);

    return redirect('/dashboard')->with('message','File upload complete!');
		
		
	}

	public function deletef($file_id)
	{

			$file = DB::table('files')->where('id', $file_id)->first();
			Storage::delete('public/teacher_docs/'.$file->name);

			$deletedRows = DB::table('files')->where('id',$file_id)->delete();
			if($deletedRows)
			return redirect('/dashboard')->with('message','File deleted');
			else
				return redirect('/dashboard')->with('message','File Not deleted');

	}

	public function userfiles()
	{
		$user=Auth::user();
		$id=$user->id;


		$ufiles = DB::table('files')->where('Uid', $id)->get();
		

		//$url = Storage::url('storage/teacher_docs',Auth()->id().'_'.$file->name);

		$files = array(

				'name' =>	array(),
				'type' =>	array(),
				'id' =>	array(),
				'url' =>	array(),
				'title' =>	array(),
				'subject'=>array(),
				'privacy'=> array(),
			   );

		foreach ($ufiles as $file) {
			if($file->type==0)
			{
				$state="Notes";
			}
			else if ($file->type==1) {
					$state="Help Material";
			} else {
					$state="Practice Material";
			}
			if($file->privacy==0)
			{
				$priv="Onley Me";
			}
			else if ($file->privacy==1) {
					$priv="Specific Users";
			} else {
					$priv="Public";
			}
				
			array_push($files['type'], $state);

			array_push($files['id'], $file->id);
			array_push($files['title'], $file->title);
			$url= Storage::url('public/teacher_docs/'.$file->name);
			array_push($files['url'], $url);
			array_push($files['name'], $file->name);
			array_push($files['subject'], $file->subject);
			array_push($files['privacy'], $priv);
			
		}
		

		return view('/dashboard/userFiles')->with('files',$files);

	}

	
	public function Doc()
	{


		return view('/dashboard/dashboard/upload-doc');
	}


    //
}
