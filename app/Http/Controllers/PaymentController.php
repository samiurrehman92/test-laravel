<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\smartcards;
use App\transactions;
use App\users;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
		$id = Auth::user()->id;
		$number=$req->input('number');
		$cards=Db::table('smartcards')->select('*')->get();
		//print_r($cards);
		$value;
		//die();
		for ($x = 0; $x < sizeof($cards); $x++) {
			if($cards[$x]->number==$number)
			{
						$value=$cards[$x]->value;
						$credit=Db::table('users')->select('credit')->where('id',$id)->first();
						$credit1=$credit->credit+$value;
						$data2=array('credit'=>$credit1);
						DB::table('users')->where('id', '=', $id)->update($data2);
						$smartcards=smartcards::find($cards[$x]->id);
						$smartcards->delete();
		$data3=array('user_id'=>$id,'detail'=>"Loaded(Smart-Card)",'amount'=>$value);
		DB::table('transactions')->insert($data3);
							return redirect('/dashboard')->with('message', 'card loaded of '.$value);
			}
			
                                             }  

		
    }
	
	public function addcard(Request $req)
    {
		return view('dashboard.add-cards');
    }
	public function generate(Request $req)
    {
		$cards=$req->input('cards');
		$amount=$req->input('amount');
		
		for ($x = 0; $x <$cards; $x++) 
		{
			
		$number= rand(100000000,999999999);	
        $data=array('number'=>$number,'value'=>$amount);
		DB::table('smartcards')->insert($data);
}
		
		return redirect('/dashboard')->with('message', 'Cards Generated');
    }
}
