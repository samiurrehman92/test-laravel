<?php

namespace App\Http\Controllers;

//use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Request;
use App\orderrequest;
use App\orderfeedback;
use App\teacher;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class orderRequestController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
		$teacher = DB::table('users')
            ->select('id','name','teacher')
            ->where('id', '=', $user_id)
            ->first();
			
		$teacher->profile_box = app('App\Http\Controllers\usersController')->get_profile_box($user_id);

		if($teacher->teacher==0)
			return redirect('dashboard')->with('message','You can not send order request to students.');
		else
			return view('dashboard.create-order-request')->with('teacher',$teacher);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($order_id)
    {
			$orderrequest=Db::table('orderrequest')->where('id',$order_id)->first();
			$orderrequest->user=app('App\Http\Controllers\usersController')->getprofile($orderrequest->User_ID);
			$orderrequest->profile_box=app('App\Http\Controllers\usersController')->get_profile_box($orderrequest->User_ID);
		return view('dashboard.respond-to-order-request',['orderrequest'=>$orderrequest]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
		//$id=auth::id;
        //$id = Auth::user()->id();
		$id = Auth::user()->id;
		$teacher=$req->input('teacher');
		$ordertype=$req->input('ordertype');
		$course=$req->input('course');
		$description=$req->input('description');
		$duration=$req->input('duration');
		$price=$_REQUEST['price'];
		
		$data=array('User_ID'=>$id,'teacher'=>$teacher,'ordertype'=>$ordertype,'course'=>$course,'description'=>$description,'duration'=>$duration,'price'=>$price);
		DB::table('orderrequest')->insert($data);
		
		return redirect('/dashboard')->with('message', 'Order request sent!');
    }
	public function Respond(Request $req)
    {
		$id = Auth::user()->id;
		$orderid=$req->input('orderid');
		$ordertype=$req->input('ordertype');
		$course=$req->input('course');
		$duration=$req->input('duration');
		$price=$_REQUEST['price'];
		 if (isset($_POST['Accept'])) {
        $status=2;
		$data=array('ordertype'=>$ordertype,'course'=>$course,'duration'=>$duration,'price'=>$price,'status'=>$status);
		DB::table('orderrequest')->where('id', '=', $orderid)->update($data);
    }
    elseif (isset($_POST['Reject'])) {
        $status=0;
		$data=array('status'=>$status);
		DB::table('orderrequest')->where('id', '=', $orderid)->update($data);
    }
		
		
		
		
		return redirect('/dashboard')->with('message', 'Order request responded!');
    }
	
	public function completeOrder($order_id)
	{
		$orderrequest=Db::table('orderrequest')->where('id',$order_id)->first();
		$orderrequest->profile_box=app('App\Http\Controllers\usersController')->get_profile_box($orderrequest->teacher);
		return view('dashboard.complete-order')->with('orderrequest',$orderrequest);
	}
	public function createOrder($order_id)
	{
		$orderrequest=Db::table('orderrequest')->where('id',$order_id)->first();
		$orderrequest->user=app('App\Http\Controllers\usersController')->getprofile($orderrequest->User_ID);
		$orderrequest->profile_box=app('App\Http\Controllers\usersController')->get_profile_box($orderrequest->teacher);

		return view('dashboard.create-order',['orderrequest'=>$orderrequest]);
		
	}
	
	
	public function checkout(Request $req)
    {
		$id = Auth::user()->id;
		$orderid=$req->input('orderid');
		$payment=$req->input('payment');

		$orderrequest=Db::table('orderrequest')->where('id',$orderid)->first();
		$orderrequest->user=app('App\Http\Controllers\usersController')->getprofile($orderrequest->User_ID);

		if($payment=="Smart-Ed Card")
		{
			if($orderrequest->user['credit']<$orderrequest->price)
				return redirect('/dashboard')->with('message', 'Insufficient balance, please load credit!');
			else
				DB::table('users')->where('id', '=', $orderrequest->user['id'])->update(['credit'=>$orderrequest->user['credit']-$orderrequest->price]);
			
			$status=3;
			$data=array('status'=>$status,'payment'=>$payment);
			DB::table('orderrequest')->where('id', '=', $orderid)->update($data);	
	
		$orderprice=Db::table('orderrequest')->select('price')->where('id',$orderid)->first();
		$data3=array('user_id'=>$id,'detail'=>"Amount deducted for Order # ".$orderid. ' ('.$payment.')','amount'=>"-".$orderprice->price);
		DB::table('transactions')->insert($data3);
	
			return redirect('/dashboard')->with('message', 'New order placed successfully!');
		}
		else if($payment=="Credit/Debit Card")
		{
			$status=3;
			$data=array('status'=>$status,'payment'=>$payment);
			DB::table('orderrequest')->where('id', '=', $orderid)->update($data);						


		$orderprice=Db::table('orderrequest')->select('price')->where('id',$orderid)->first();
		$data3=array('user_id'=>$id,'detail'=>"Payment loaded for Order # ".$orderid. ' ('.$payment.')','amount'=>$orderprice->price);
		DB::table('transactions')->insert($data3);			

		$data3=array('user_id'=>$id,'detail'=>"Amount deducted for Order # ".$orderid,'amount'=>"-".$orderprice->price);
		DB::table('transactions')->insert($data3);
			
			return redirect('/dashboard')->with('message', 'New order placed successfully!');
		}

		else return redirect('/dashboard')->with('message', 'ERROR: Unkown Payment Method!');

    }
	
	public function AcceptDelivery(Request $req)
	{
		$id = Auth::user()->id;
		$orderid=$req->input('orderid');
		$feedback=$req->input('feedback');
		//$revision=$req->input('revision');
		$rating=$req->input('rating');
		$Teacherid=Db::table('orderrequest')->select('teacher')->where('id',$orderid)->first();
		//echo $feedback;
		//echo $revision;
		//echo $rating;
		//echo $Teacherid->teacher;
		$idd=$Teacherid->teacher;
		$status=4;
		$data2=array('status'=>$status);
		DB::table('orderrequest')->where('id', '=', $orderid)->update($data2);
		//die();
		$data=array('orderid'=>$orderid,'feedback'=>$feedback);
		DB::table('orderfeedback')->insert($data);
		
		$data1=array('rating'=>$rating);
		DB::table('teacher')->where('Teacher_ID', '=', $idd)->update($data1);
		
		$orderprice=Db::table('orderrequest')->select('price')->where('id',$orderid)->first();
		$eightypercent=$orderprice->price*(80/100);
		$twentypercent=$orderprice->price*(20/100);
		$data3=array('user_id'=>$Teacherid->teacher,'detail'=>"Amount recieved for orderID#".$orderid,'amount'=>$eightypercent);
		DB::table('transactions')->insert($data3);
		$newcredit=Db::table('users')->select('credit')->where('id',$Teacherid->teacher)->first();
		$newcredit->credit=$newcredit->credit+$eightypercent;
		DB::table('users')->where('id', '=', $Teacherid->teacher)->update(array('credit'=>$newcredit->credit));
		
		$newcreditadmin=Db::table('users')->select('credit')->where('id',0)->first();
		$newcreditadmin->credit=$newcreditadmin->credit+$twentypercent;
		DB::table('users')->where('id', '=', 0)->update(array('credit'=>$newcreditadmin->credit));
		$data4=array('user_id'=>0,'detail'=>"Amount recieved for Order # ".$orderid,'amount'=>$twentypercent);
		DB::table('transactions')->insert($data4);
		return redirect('/dashboard')->with('message', 'Order completed!');
	}
	public function RequestRevision(Request $req)
	{
		$orderid=$req->input('orderid');
		//$feedback=$req->input('feedback');
		$revision=$req->input('revision');
		
		$status=5;
		$data2=array('status'=>$status);
		DB::table('orderrequest')->where('id', '=', $orderid)->update($data2);
		
		$data=array('orderid'=>$orderid,'revision'=>$revision);
		DB::table('orderfeedback')->insert($data);
		return redirect('/dashboard')->with('message', 'requested revision!');
	}
	public function view_all()
	{
		if(Auth::user()->id!=0)
			return redirect('/dashboard')->with('message','Unauthorized access!');
       $statuss="3,6";
		$orderrequests = DB::table('orderrequest')
            ->where('id','>', '0')->wherein('status', [3,6])
			->get();
			foreach($orderrequests as $orderrequest)
			{
				if($orderrequest->status==1)
					$orderrequest->status="requested";
				else if($orderrequest->status==2)
					$orderrequest->status="responded";
				else if($orderrequest->status==3)
					$orderrequest->status="active";
				else if($orderrequest->status==4)
					$orderrequest->status="completed";
				else if($orderrequest->status==5)
					$orderrequest->status="revision";
				else if($orderrequest->status==6)
					$orderrequest->status="suspended";
				
                $orderrequest->student_name=Db::table('users')->select('name')->where('id',$orderrequest->User_ID)->first()->name;
                $orderrequest->teacher_name=Db::table('users')->select('name')->where('id',$orderrequest->teacher)->first()->name;

			}
		return view('dashboard.orders-directory',['orderrequests'=>$orderrequests]);
	}

   public function suspend($order_id)
    {
		$orderrequest=orderrequest::find($order_id);
		$orderrequest->status=6;
		$orderrequest->save();
		return redirect('/dashboard/all-orders')->with('message','order suspended');
    }
	public function activate($order_id)
    {
		$orderrequest=orderrequest::find($order_id);
		$orderrequest->status=3;
		$orderrequest->save();
		return redirect('/dashboard/all-orders')->with('message','order activated');
    }
	
    public function matrials(Request $request)
    {
    	$data = $request->all();
    	$oid= $data['order_id'];
    	$order = DB::table('orderrequest')->where('id', '=', $data['order_id'])->first();
    	$student=$order->User_ID;
    	$files= $data['fileids'];
    	

    	foreach ($files as $file) {
    		$tfiles = DB::table('files')->where('id', $file)->first();
    		$students=$tfiles->sId;
    		$students=$students.','.$student;

    		DB::table('files')
            ->where('id', $file)
            ->update([
			'sId' => $students,
			]);
    		
    	}

		$data1=array('status'=>10);
		DB::table('orderrequest')->where('id', '=', $oid)->update($data1);

    	return redirect('/dashboard');

    }


    public function show($order_id)
	{
		$order = DB::table('orderrequest')->where('id', '=', $order_id)->first();
		$user = Auth::user();
		$id = $user->id;
		if(strcmp('Help Material',$order->ordertype) ==0 || strcmp('Practice Material',$order->ordertype)==0 )
		{
			if($user->teacher)
			{

				$ufiles = DB::table('files')->where('Uid', $id)->get();
		

		//$url = Storage::url('storage/teacher_docs',Auth()->id().'_'.$file->name);

		$files = array(

				'name' =>	array(),
				'type' =>	array(),
				'id' =>	array(),
				'url' =>	array(),
				'title' =>	array(),
				'subject'=>array(),
				'privacy'=> array(),
			   );

		foreach ($ufiles as $file) {
			if($file->type==0)
			{
				$state="Notes";
			}
			else if ($file->type==1) {
					$state="Help Material";
			} else {
					$state="Practice Material";
			}
			if($file->privacy==0)
			{
				$priv="Onley Me";
			}
			else if ($file->privacy==1) {
					$priv="Specific Users";
			} else {
					$priv="Public";
			}
				
			array_push($files['type'], $state);

			array_push($files['id'], $file->id);
			array_push($files['title'], $file->title);
			$url= Storage::url('public/teacher_docs/'.$file->name);
			array_push($files['url'], $url);
			array_push($files['name'], $file->name);
			array_push($files['subject'], $file->subject);
			array_push($files['privacy'], $priv);
			
		}



				return view('/dashboard/material-order')->with(['order'=>$order,'user'=>$user,'files'=>$files]);
			}
			else
			{
				return view('/dashboard')->with('message','Not Allowed');
			}
		}
		if($order->remaining_time=="00:00:00")
			return redirect('/dashboard/completed-order/'.$order->id);

		return view('dashboard.order')->with(['order'=>$order,'user'=>$user]);
		
	}
    public function update_remaining_time()
	{
		$data=array('remaining_time'=>$_POST['timeleft']);
		DB::table('orderrequest')->where('id', '=', $_POST['order_id'])->update($data);
		echo $_POST['timeleft'];
	}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
