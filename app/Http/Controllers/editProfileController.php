<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\teacher;
use App\student;
use App\User;
use Hash;

class editProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
	{
		$user=Auth::user();
		$id=$user->id;

		if ($user->teacher) 
		{
			$email=$user->email;
			$name=$user->name;

			$exists = file_exists('storage/avatars/'.Auth()->id().'dp.jpg');
			if(!$exists)
				$url="/images/user.png";
			else
				$url = Storage::url('/avatars/'.Auth()->id().'dp.jpg');
			
			$teacher=teacher::where('UserId','=',$id)->first();
			
			if(count($teacher)==1)
			{
					$contact=$teacher->contact;
					$address=$teacher->address;
					$city=$teacher->city;
					$country=$teacher->country;
					$qualification=$teacher->Academic_Qualification;
					$charity=$teacher->TeachForCharity;
					$CNIC=$teacher->CNIC;
			}
			else
			{
				$teacher = new teacher;
				$teacher->UserId=$user->id;
				$teacher->save();
				
				$contact="";
				$address="";
				$city="";
				$country="";
				$education="";
			}

			$data = array(
				'name'=>$name,
				'email'=>$email,
				'contact'=>$contact,
				'address'=>$address,
				'city'=>$city,
				'country'=>$country,
				'qualification'=>$qualification,
				'cnic'=>$CNIC,
				'charity'=>$charity,
				'url'=>$url,
				);
			return view('dashboard/edit-profile-teacher')->with('data', $data);			
		}
		else
		{

			$email=$user->email;
			$name=$user->name;
			
			$exists = file_exists('storage/avatars/'.Auth()->id().'dp.jpg');
			if(!$exists)
				$url="/images/user.png";
			else
				$url = Storage::url('/avatars/'.Auth()->id().'dp.jpg');
			
			$student=student::where('UserId','=',$id)->first();
			if(count($student)==1)
			{
					$contact=$student->contact;
					$address=$student->address;
					$city=$student->city;
					$country=$student->country;
					$education=$student->education;
			}
			else
			{
				$teacher = new student;
				$teacher->UserId=$user->id;
				$teacher->save();
				
				$contact="";
				$address="";
				$city="";
				$country="";
				$education="";
			}

			$data = array(
				'name'=>$name,
				'email'=>$email,
				'contact'=>$contact,
				'address'=>$address,
				'city'=>$city,
				'country'=>$country,
				'education'=>$education,
				'url'=>$url,
				);
			return view('dashboard/edit-profile-student')->with('data', $data);
		}
	}
	public function updateuser(Request $request)
	{
		$data = $request->all();
		
		$user=Auth::user();
		$id=$user->id;
		if ($user->teacher) 
		{
			if(!isset($data['charity']))
				$data['charity']=0;

			DB::table('users')
            ->where('id', $user->id)
            ->update([
			'name' => $data['name'],
			]);

			DB::table('teacher')
            ->where('UserId', $user->id)
            ->update([
			'cnic' => $data['cnic'],
			'contact' => $data['contact'],
			'address' => $data['address'],
			'Academic_Qualification' => $data['education'],
			'TeachForCharity' => $data['charity'],
			'city' => $data['city'],
			'country' => $data['country'],
			]);			
			return redirect('/dashboard')->with('message','Profile updated!');
		}
		else
		{
			
			DB::table('users')
            ->where('id', $user->id)
            ->update([
			'name' => $data['name'],
			]);
			
			DB::table('student')
            ->where('UserId', $user->id)
            ->update([
			'contact' => $data['contact'],
			'address' => $data['address'],
			'education' => $data['education'],
			'city' => $data['city'],
			'country' => $data['country'],
			''=>''
			]);
			
			return redirect('/dashboard')->with('message','Profile updated!');

		}
	}
	
	public function update_password(Request $request)
	{
		$data = $request->all();
		
		$user=Auth::user();
		if (Auth::validate(['email' => $user->email, 'password' => $data['curr_pass']]))
		{
			$obj_user = User::find($user->id);
			$obj_user->password = Hash::make($data['new_pass']);
			$obj_user->save();
			return redirect('/dashboard')->with('message','Password updated!');
		}
		else
			return redirect('/dashboard')->with('message','Incorrect current password!');
	}
	
}
