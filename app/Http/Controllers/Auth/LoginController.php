<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Abraham\TwitterOAuth\TwitterOAuth;
use Auth;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);		
    }

    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }
	
	public function show(Request $request)
	{
		if(Auth::check())
			return redirect('/dashboard')->with('message','Welcome back!');

		return view('login');
	}

	public function social_login()
    {
		$user_id = User::select('id')->where('email', $_POST['email'])->first();
		if($user_id)
		{
			if(Auth::loginUsingId($user_id->id))
				echo "1";
		}
		else {
			$newUser= User::create([
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => bcrypt("123"),
            'teacher' => 1        
			]);
			echo "2";
		}
    }
	public function show_twitter_login()
	{
			session_start();
			$twitteroauth = new TwitterOAuth("s0xxEl5nupPWJoMnCrP3GHRyf", "5TjwUSYQJhCCeYFPbhfcXAWmnWu5Ct3zUuToKpCpZ5FSnkth5L");
			 
			// request token of application
			$request_token = $twitteroauth->oauth(
				'oauth/request_token', [
					'oauth_callback' => "http://ec2-35-167-202-49.us-west-2.compute.amazonaws.com/social_login/twitter_callback"
				]
			);
			 
			// throw exception if something gone wrong
			if($twitteroauth->getLastHttpCode() != 200) {
				throw new \Exception('There was a problem performing this request');
			}
			 
			// save token of application to session
			$_SESSION['oauth_token'] = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
			 
			// generate the URL to make request to authorize our application
			$url = $twitteroauth->url(
				'oauth/authorize', [
					'oauth_token' => $request_token['oauth_token']
				]
			);
			
			// and redirect
			header('Location: '. $url);
			echo '<script>window.location="'.$url.'"</script>';
	}
	
	public function twitter_callback(){
		
		session_start();
		$oauth_verifier = filter_input(INPUT_GET, 'oauth_verifier');
 
		if (empty($oauth_verifier) ||
			empty($_SESSION['oauth_token']) ||
			empty($_SESSION['oauth_token_secret'])
		) {
			// something's missing, go and login again
			header('Location: /login');
			echo '<script>window.location="/login"</script>';
		}
		
		// connect with application token
		$connection = new TwitterOAuth(
			"s0xxEl5nupPWJoMnCrP3GHRyf",
			"5TjwUSYQJhCCeYFPbhfcXAWmnWu5Ct3zUuToKpCpZ5FSnkth5L",
			$_SESSION['oauth_token'],
			$_SESSION['oauth_token_secret']
		);
		 
		// request user token
		$token = $connection->oauth(
			'oauth/access_token', [
				'oauth_verifier' => $oauth_verifier
			]
		);

		$twitter = new TwitterOAuth(
			"s0xxEl5nupPWJoMnCrP3GHRyf",
			"5TjwUSYQJhCCeYFPbhfcXAWmnWu5Ct3zUuToKpCpZ5FSnkth5L",
			$token['oauth_token'],
			$token['oauth_token_secret']
		);
		$content = $twitter->get("account/verify_credentials", ['include_email' => 'true']);
		$user_id = User::select('id')->where('email', $content->email)->first();
		if($user_id)
		{
			if(Auth::loginUsingId($user_id->id))
			{
				header('Location: /dashboard');
				echo '<script>window.location="/dashboard"</script>';
			}
			else echo "0";
		}
		else {
			$newUser= User::create([
            'name' => $content->name,
            'email' => $content->email,
            'password' => bcrypt("123"),
            'teacher' => 1            
			]);

		return redirect('/login')->with('message','New user registered, please log in!');
		
/*		if(Auth::loginUsingId($newuser->id))
			{
				header('Location: /dashboard');
				echo '<script>window.location="/dashboard"</script>';
			}
			else echo "0";
*/

		};
	}
}
