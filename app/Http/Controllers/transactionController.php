<?php

namespace App\Http\Controllers;

//use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Request;
use App\transactions;
use App\orderfeedback;
use App\teacher;
use Illuminate\Support\Facades\DB;


class transactionController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($order_id)
    {
			
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function withdrawal()
    {
		$id=Auth::user()->id;
       $credit=Db::table('users')->select('credit')->where('id',$id)->first();
	   
	   $data3=array('user_id'=>$id,'detail'=>"Amount requested for withdrawal",'amount'=>"-".$credit->credit);
		DB::table('transactions')->insert($data3);
		
		$data1=array('credit'=>0);
		DB::table('users')->where('id', '=', $id)->update($data1);
		return redirect('dashboard')->with('message','Withdrawal successful!');
    }
	
	public function view_all()
	{
		$id=Auth::user()->id;
       
		$transactions = DB::table('transactions')->where('user_id','=',$id) ->orderBy('date_time', 'desc')->get();
		return view('dashboard.view-transactions',['transactions'=>$transactions]);
	}

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
