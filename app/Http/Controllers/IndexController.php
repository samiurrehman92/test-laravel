<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = array();
		
		$countusers = DB::table('users')->count();
		$data['countusers']=$countusers;
		
		$countteachers = DB::table('users')->where('teacher',1)->count();
		$data['countteachers']=$countteachers;
		
		$teachers = DB::table('users')->where('teacher',1)->limit(5)->get();
		$data['teachers']=$teachers;

		return view('index')->with('data',$data);
    }
}
