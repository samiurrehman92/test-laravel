<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
//use Request;
use App\User;
use App\teacher;
use App\student;
use Illuminate\Support\Facades\DB;


class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function myfiles()
    {
        $user=Auth::user();
        $id=$user->id;
        $flag=false;
        $rfiles = array(
                'name' =>   array(),
                'teacher' =>   array(),
                'teacherID' =>   array(),
                'type' =>   array(),
                'id' => array(),
                'url' =>    array(),
                'title' =>  array(),
                'subject'=>array(),
                'privacy'=> array(),
               );
        $files = DB::table('files')->get();
        foreach ($files as $file)
        {
            $userids=$file->sId;
            $users = explode(",", $userids);
            foreach ($users as $user) {
                if($id==$user)
                {

                    $teacher = DB::table('users')->where('id',$file->Uid)->first();

                    if($file->type==0)
                    {
                        $state="Notes";
                    }
                    else if ($file->type==1) {
                            $state="Help Material";
                    } else {
                            $state="Practice Material";
                    }
                    if($file->privacy==0)
                    {
                        $priv="Onley Me";
                    }
                    else if ($file->privacy==1) {
                            $priv="Specific Users";
                    } else {
                            $priv="Public";
                    }
                        
                    array_push($rfiles['type'], $state);

                    array_push($rfiles['id'], $file->id);
                    array_push($rfiles['teacherID'], $file->Uid);
                    array_push($rfiles['teacher'], $teacher->name);
                    array_push($rfiles['title'], $file->title);
                    $url= Storage::url('public/teacher_docs/'.$file->name);
                    array_push($rfiles['url'], $url);
                    array_push($rfiles['name'], $file->name);
                    array_push($rfiles['subject'], $file->subject);
                    array_push($rfiles['privacy'], $priv);
                    break;
                }
            }
           
            
        } 

        return view('/dashboard/myfiles')->with('files',$rfiles);
    }


    
	public function showProfile($user_id)
    {
		$user= $this->getprofile($user_id);
       return view('profile')->with('profile',$user);
    }

	public function get_profile_box($user_id)
	{
		$user = $this->getprofile($user_id);
		if($user['teacher'])
			$description = "<b>Qualification: </b>".$user['qualification']."<br><b>Courses: </b>" . $user['courseToteach'];
		else
			$description = "<b>Education: </b>".$user['education'];
		
		$box_html = '<div class="search-result-item" style="margin:0px;    margin-bottom: 15px;" >
			<div class="col-md-2">
				<img src="'.$user['image'].'"></img>
			</div>
			<div class="col-md-9">
				<a href="/profile/'.$user['id'].'"><h6>'.$user['name'].'</h6></a>
				<span>'.$description.'</span>
				<span>Rating '.$user['rating'].' stars</span>
			</div>
			<div class="col-md-1 action-buttons">
				<a href="/dashboard/chat/'.$user['id'].'"><p class="fa fa-comments"></p></a>
				<a style="margin-top:12px;" href="/dashboard/create-order-request/'.$user['id'].'"><p class="fa fa-rocket"></p></a>
			</div>
		</div>';
		return $box_html;
	}
	
	public function getprofile($id)
	{
        //$user= user::find($id);
         $user=DB::table('users')->where('id', '=', $id)->first();
		$exists = file_exists('storage/avatars/'.$id.'dp.jpg');
		if(!$exists)
			$url="/images/user.png";
		else
			$url = Storage::url('/avatars/'.$id.'dp.jpg');	
		
        if($user->teacher)
        {
            $teacher=teacher::where('UserId', $id)->first();
            if(count($teacher)==1)
            {
                $data = array(
				"id"=>$user->id,
                 "name"=>$user->name, 
                 "email"=>$user->email,
                 "credit"=>$user->credit,
                 "image"=>$url,
                 "teacher"=>$user->teacher,
                 "TeacherId"=>$teacher->id,
                 "qualification"=>$teacher->Academic_Qualification,
                 "courseToteach"=>$teacher->CoursesToTeach,
                 "standards"=>$teacher->standards,
                 "experience"=>$teacher->Experience,
                 "charity"=>$teacher->TeachForCharity,
                 "cnic"=>$teacher->CNIC,
                 "rating"=>$teacher->rating,
                 "contact"=>$teacher->contact,
                 "address"=>$teacher->address,
                 "city"=>$teacher->city,
                 "country"=>$teacher->country,
                 "status"=>1
                 );
                return $data;

            }
            $data = array("id"=>$user->id,
                 "name"=>$user->name, 
                 "email"=>$user->email,
                "status"=>0
                 );
            return $data;
        }
        else
        {
            $student=student::where('UserId', $id)->first();
            if(count($student)==1)
            {
                $data = array("id"=>$user->id,
                 "name"=>$user->name, 
                 "email"=>$user->email,
                 "credit"=>$user->credit,
                 "image"=>$url,
                 "teacher"=>$user->teacher,
                 "contact"=>$student->contact,
                 "country"=>$student->country,
                 "city"=>$student->city,
                 "address"=>$student->address,
                 "rating"=>$student->rating,
                 "education"=>$student->education,
                 "status"=>1
                 );
                return $data;
            }
            else
            {
            //user is admin
            $data = array("id"=>$user->id,
                 "name"=>$user->name, 
                 "email"=>$user->email,
                 "teacher"=>$user->teacher,
                "status"=>3
                 );
            return $data;
            }
        }
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Deactivate(Request $request)
    {$id = Auth::user()->id;
        $users=users::find($id);
		$users->status=2;
		$users->save();
	 echo $id;
    }

	    public function Suspend($user_id)
    {
		$users=user::find($user_id);
		$users->status=3;
		$users->save();
		if(Auth::user()->id==$user_id)
		{
			Auth::logout();
			return redirect('/')->with('message','User suspended');
		}
		return redirect('/dashboard/all-users')->with('message','User suspended');
    }
	
    public function activate($user_id)
    {
		$users=user::find($user_id);
		$users->status=1;
		$users->save();
		return redirect('/dashboard/all-users')->with('message','User activated');
    }
	
    public function deleteuser($user_id)
    {
		$users=user::find($user_id);
		$users->delete();
		return redirect('/dashboard/all-users')->with('message','User deleted');
    }
	
	public function view_all()
	{
		if(Auth::user()->id!=0)
			return redirect('/dashboard')->with('message','Unauthorized access!');

		$users = DB::table('users')
            ->where('id','>', '0')
			->get();
			foreach($users as $user)
				if($user->status==1)
					$user->status="Active";
				else if($user->status==2)
					$user->status="Deactiaved";
				else if($user->status==3)
					$user->status="Suspended";

		return view('dashboard.users')->with('users',$users);
	}
	
	public function showSettings()
	{
		return view('dashboard.user-settings');
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
