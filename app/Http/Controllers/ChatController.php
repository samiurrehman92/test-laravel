<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


use App\User;
use Auth;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		date_default_timezone_set("Asia/Karachi");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
		$url="/";
		if(Auth::user()->id==$user_id)
			return redirect('/dashboard')->with('message','You cannot chat with yourself!');

		$chat_user = User::select('*')->where('id', $user_id)->first();		

		$data = array(
			'user2'=> $chat_user
        );
        return view('/dashboard/chat')->with($data);
    }

    public function admin()
    {
		$chat_user = User::select('*')->where('id', '0')->first();		

		$data = array(
			'user2'=> $chat_user
        );
        return view('/dashboard/chat')->with($data);
    }
	
	public function sendMsg()
	{
		date_default_timezone_set("Asia/Karachi");
		DB::table('message')->insert(
			[
			'Sender_UserID' => $_POST['sender_id'],
			'Reciever_UserID' => $_POST['reciever_id'],
			'Message_Text'=>$_POST['msg'],
			'timestamp'=>date("Y-m-d H:i:s")
			]
		);
	}
	
	public function getMsg()
	{
		$user2 = User::select('*')->where('id', $_REQUEST['user2'])->first();

		$msgs = DB::table('message')
			 ->select()
			 ->whereIn('Sender_UserID', array(Auth::user()->id,$user2->id))
			 ->whereIn('Reciever_UserID', array(Auth::user()->id,$user2->id))
			 ->where('MessageID', '>', $_REQUEST['lastTimeID'])
			 ->get();		
			 
		foreach($msgs as $msg)
		if(Auth::user()->id==$msg->Sender_UserID)
		echo '
			<li id="msg-'.$msg->MessageID.'" class="left clearfix">
				<span class="chat-img pull-left">
					<img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
				</span>
				<div class="chat-body clearfix">
					<div class="header">
						<strong class="primary-font">'.Auth::user()->name.'</strong>
						<small class="pull-right text-muted">
							<i class="fa fa-clock-o fa-fw"></i> <span class="timestamp" timestamp="'.$msg->timestamp.'">just now</span>
						</small>
					</div>
					<p>
						'.$msg->Message_Text.'
					</ps
				</div>
			</li>		
		';
		else echo '
		<li id="msg-'.$msg->MessageID.'" class="right clearfix">
			<span class="chat-img pull-right">
				<img src="http://placehold.it/50/FA6F57/fff" alt="User Avatar" class="img-circle">
			</span>
			<div class="chat-body clearfix">
				<div class="header">
					<small class=" text-muted">
						<i class="fa fa-clock-o fa-fw"></i> <span class="timestamp" timestamp="'.$msg->timestamp.'">just now</span>
					</small>
					<strong class="pull-right primary-font">'.$user2->name.'</strong>
				</div>
				<p style="text-align:right;">
						'.$msg->Message_Text.'
				</p>
			</div>
		</li>
		';
	}
}
