<?php 
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GlobalComposer {

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
		$dashboard = null;
		if(Auth::check())
		{
			$dashboard = array();
			$user_id = Auth::user()->id;

			$dashboard['user']=Auth::user();
			
			if($user_id==0)
				$dashboard['is_admin']=1;
			else
				$dashboard['is_admin']=0;
			
			$dashboard['credit']=Auth::user()->credit;

				if(Auth::user()->teacher)
			{
				//get orders for teacher
				$orders=Db::table('orderrequest')->where('teacher',$user_id)->get();
				foreach($orders as $id => $order)
				{
					$order->student_name=Db::table('users')->select('name')->where('id',$order->User_ID)->first()->name;
					if($order->status==2 ||  $order->status==10) //already responded
						unset($orders[$id]);
				}
			}
			else
			{
				//get orders for students
				$orders=Db::table('orderrequest')->where('User_ID',$user_id)->get();
				foreach($orders as $id => $order)
				{
					$order->student_name=Db::table('users')->select('name')->where('id',$order->teacher)->first()->name;

					if($order->status==1) //already requested
						unset($orders[$id]);
				}
			}
			
			//standardize all orders
			foreach($orders as $id => $order)
			{
				if($order->status==0 || $order->status==4 )
					unset($orders[$id]);
				else if($order->status==1)
				{
					$order->title="Order requested from " . $order->student_name;
					$order->status="requested";
					$order->url="/dashboard/respond-to-order-request/".$order->id;
				}
				else if($order->status==2)
				{
					$order->title="Order requested from " . $order->student_name;
					$order->status="pending";
					$order->url="/dashboard/create-order/".$order->id;
				}
				else if($order->status==3)
				{
					$order->title="Active order with ". $order->student_name;
					$order->status="active";
					$order->url="/dashboard/order/".$order->id;
				}
				else if($order->status==5)
				{
					$order->title="Order dispute order with ". $order->student_name;
					$order->status="dispute";
					$order->url="/dashboard/order/".$order->id;
				}				
				else if($order->status==10)
				{
					$order->title="Delivered order from ". $order->student_name;
					$order->status="completed";
					$order->url="/dashboard/completed-order/".$order->id;
				}				
			}

			$dashboard['orders'] = $orders;
			
			$sql = "SELECT MAX(MessageID) AS id FROM
				(SELECT MessageID, Sender_UserID AS id_with
				FROM message
				WHERE Reciever_UserID = ".$user_id."
				UNION ALL
				SELECT MessageID, Reciever_UserID AS id_with
				FROM message
				WHERE Sender_UserID = ".$user_id.") t
				GROUP BY id_with";

			$result = DB::select($sql);
			$result = array_map(function ($value) {
				return $value->id;
			}, $result);

			$convos = Db::table('message')->select('*')->whereIn('MessageID',$result)->get();
			foreach($convos as $convo)
			{
				if($convo->Sender_UserID==$user_id)
				{
					$convo->user_id = $convo->Reciever_UserID;
					$convo->user_name = Db::table('users')->select('name')->where('id',$convo->Reciever_UserID)->first()->name;
				}
				else
				{
					$convo->user_id = $convo->Sender_UserID;
					$convo->user_name = Db::table('users')->select('name')->where('id',$convo->Sender_UserID)->first()->name;
				}
			}
			$dashboard['latest_convos'] = $convos;
		}
        $view->with('dashboard', $dashboard);
    }

}