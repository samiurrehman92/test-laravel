<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
	public $table = "users";
    protected $fillable =['id','name','email','password','teacher','status'];
}
