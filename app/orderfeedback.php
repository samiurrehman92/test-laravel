<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderfeedback extends Model
{
	public $table = "orderfeedback";
    protected $fillable =['id','orderid','feedback','revision'];
}
