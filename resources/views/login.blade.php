<!DOCTYPE html>
<html lang="en">
	<head>

    <title>Smart-Ed | Learn Online Now!</title>
	
	<?php require_once('index_template/head.blade.php'); ?>
	<meta name="google-signin-client_id" content="267920430335-akvtomj2fqdnhjh2o3bjsb3vp8ba1r03.apps.googleusercontent.com">

  </head>
  <body>  
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1936417723254710',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView(); 
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <!-- Header Section Start -->
	<?php require_once('index_template/header.blade.php'); ?>
    <!-- Header Section End -->

	<section id="page">
	<div class="row" style="padding-top:100px;" >
		
		@if($errors->any())
			<div class="alert alert-danger" align="center" style="margin-bottom:0px;">
				{{$errors->first()}}
			</div>
		@endif

		<form  class="login-form col-md-6 container" method="POST" style="border-right:2px solid #48acef; min-height:300px; padding-top:50px;">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group is-empty"><input class="form-control keyword" name="email" value="" placeholder="Email Address" type="email" required><span class="material-input"></span></div>
			</div>
			<div class="col-md-6 col-md-offset-3">
					<div class="form-group is-empty"><input class="form-control keyword" name="password" value="" placeholder="Password" type="password" required><span class="material-input"></span></div>
			</div>
			<div class="col-md-6 col-md-offset-3">
				<button class="btn btn-common btn-search btn-block"><strong>Submit</strong><div class="ripple-container"></div></button>
			 </div>
		</form>
		
		<div class="login-social col-md-6 container" style="padding-top:50px;">	
				
			<div class="col-md-6 col-md-offset-3">	

			<a class="btn btn-block btn-social btn-lg btn-facebook">
				<span class="fa fa-facebook"></span> Sign in with Facebook
			</a>
			  <br>
			  <a class="btn btn-block btn-social btn-lg btn-twitter" href="/social_login/twitter">
				<span class="fa fa-twitter"></span> Sign in with Twitter
			  </a>	
			  <br>
			  
			  <a id="btn-google" class="btn btn-block btn-social btn-lg btn-google" id="btn-google">
				<span class="fa fa-google"></span> Sign in with Google+
			  </a>			  
			</div>
			<br>
		</div>
	
	</div>
	</section>
	
    <!-- Footer Section Start -->
    <footer>
	<?php require_once('index_template/footer.blade.php'); ?>
    </footer>
    <!-- Footer Section End -->      

<script>

    window.onbeforeunload = function(e){
      gapi.auth2.getAuthInstance().signOut();
    };

function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          LoginComplete(googleUser);
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }

  
$( document ).ready(function() {
    $(".btn-facebook").click(function(){
		FB.getLoginStatus(function(res){
			if( res.status == "connected" )
			{
				LoginComplete('facebook');
			}
			else {
				FB.login(function(response) {
				LoginComplete('facebook');
			}, {scope: 'email'});
			}
		});		
	});
	
	 gapi.load('auth2', function(){
		  auth2 = gapi.auth2.init({
			client_id: '267920430335-akvtomj2fqdnhjh2o3bjsb3vp8ba1r03.apps.googleusercontent.com',
			cookiepolicy: 'single_host_origin',
			//scope: 'additional_scope'
		  });
		  attachSignin(document.getElementById('btn-google'));
		});	
});


 function LoginComplete(type) {

	if(type=="facebook")
	{
		FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
			console.log(response);
			$.ajax({
			  method: "POST",
			  url: "/social_login",
			  data: { fb_id: response.id, name: response.name, email: response.email, _token: "<?php echo csrf_token(); ?>" },
				success: function(response){ // What to do if we succeed
					console.log(response);
					if(response=="0")
						alert("Login failed");
					else window.location="/dashboard";
				},
				error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
				alert('post failed');
				console.log(JSON.stringify(jqXHR));
					console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
				}
			})
		})
	}
	else
	{
		var profile = type.getBasicProfile();
		$.ajax({
			  method: "POST",
			  url: "/social_login",
			  data: { name: profile.getName(), email: profile.getEmail(), _token: "<?php echo csrf_token(); ?>" },
				success: function(response){ // What to do if we succeed
					console.log(response);
					if(response=="0")
						alert("Login failed");
					else window.location="/dashboard";
				},
				error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
				console.log(JSON.stringify(jqXHR));
					console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
				}
			})
	}
 }
</script>
	
</body></html>