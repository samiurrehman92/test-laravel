<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Search Teacher</h1>

						<div class="panel panel-default col-lg-3" style="padding:0px;">
							<div class="panel-heading">
								Filter Results
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form role="form">
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-user"></span>
												</span>	
												<input class="form-control" placeholder="Teacher's Name">
											</div>
											<div class="form-group">
												<label>Education Type</label>
												<div class="checkbox">
													<label>
														<input type="checkbox" value="">O' Level
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" value="">Matric
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" value="">A' Level
													</label>
												</div>
											</div>		

											<div class="form-group">
												<label>Experience in years</label>
												<input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="2"/>
											</div>

											<div class="form-group">
												<label>Rating in stars</label>
												<input id="ex2" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="5" data-slider-step="1" data-slider-value="2"/>
											</div>

											
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-building-o"></span>
												</span>	
												<select class="form-control">
													<option>City</option>
													<option>Lahore</option>
													<option>Islamabad</option>
													<option>Peshawar</option>
													<option>Karachi</option>
												</select>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-bank"></span>
												</span>	
												<select class="form-control">
													<option>Country</option>
													<option>Pakistan</option>
													<option>Iran</option>
													<option>Afghanistan</option>
													<option>India</option>
												</select>
											</div>
											<div class="form-group input-group">
												<div class="checkbox">
													<label>
														<input type="checkbox" value="">Show Online Only
													</label>
												</div>
											</div>
											<button type="submit" class="btn btn-default">Submit</button>
										</form>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>
						
						<div class="panel panel-default search-results col-lg-8" style="margin-left:15px; padding:0px;">
							<div class="panel-heading">
								Search Results
							</div>
							<div class="panel-body">
								<div class="row results-header">
									<div class="col-sm-6">
										<div class="dataTables_length" id="dataTables-example_length">
										<select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="0">Entries per page</option>
												<option value="10">10</option>
												<option value="25">25</option>
												<option value="50">50</option>
												<option value="100">100</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="0">Default Search</option>
												<option value="10">Sort by Price</option>
												<option value="25">Sort by Experience</option>
												<option value="50">Sort by Rating</option>
											</select>							
									</div>
								</div>							
								
								<div class="row results-body">
								
								<?php for($i=0; $i<8; $i++) { ?>
									<div class="search-result-item">
										<div class="col-md-2">
											<img src="/images/user.png"></img>
										</div>
										<div class="col-md-9">
											<a href="user-profile.php"><h6>Teacher Qasim</h6></a>
											<span>O Level Physics Teacher at Beaconhouse School System</span>
											<span>Physics Teacher for 5 years</span>
											<span>Rating 3 stars</span>
										</div>
										<div class="col-md-1 action-buttons">
											<a href="chat-screen.php"><p class="fa fa-comments"></p></a>
											<a href="#"><span class="glyphicon glyphicon-star"></span></a>
											<a href="chat-screen.php"><p class="fa fa-rocket"></p></a>
										</div>
									</div>
								<?php } ?>

								</div>
								
								<div class="row results-footer">
									<div class="col-sm-6">
										<div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
									</div>
									<div class="col-sm-6">
										<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">2</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4</a><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul>
										</div>
									</div>
								</div>
							
							</div>
						</div>						

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
