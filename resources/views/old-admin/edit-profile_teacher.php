<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	<?php require_once('template/head.php'); ?>
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Teacher Profile</h1>
					
						<div class="panel panel-default col-lg-5" style="padding:0px;">
							<div class="panel-heading">
								Edit Basic Profile
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<center>
											<img src="/images/user.png"></img>
											<div class="form-group">
												<label>Change Profile Image</label>
												<input type="file">
											</div>
										</center>
										<form role="form">
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-credit-card"></span>
												</span>	
												<input class="form-control" placeholder="CNIC (e.g. 35202-6969392-3)">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-user"></span>
												</span>	
												<input class="form-control" placeholder="Full Name">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-envelope-o"></span>
												</span>	
												<input class="form-control" type="email" placeholder="Email Address">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-phone"></span>
												</span>	
												<input class="form-control" type="phone" placeholder="Contact Number (e.g. +92-321-3213213)">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-home"></span>
												</span>	
												<textarea class="form-control" rows="3">Complete Address</textarea>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-building-o"></span>
												</span>	
												<select class="form-control">
													<option>City</option>
													<option>Lahore</option>
													<option>Islamabad</option>
													<option>Peshawar</option>
													<option>Karachi</option>
												</select>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-bank"></span>
												</span>	
												<select class="form-control">
													<option>Country</option>
													<option>Pakistan</option>
													<option>Iran</option>
													<option>Afghanistan</option>
													<option>India</option>
												</select>
											</div>
										</form>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
						
						<div class="panel panel-default col-lg-6 col-lg-offset-1" style="padding:0px;">
							<div class="panel-heading">
								Edit Educational Profile
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form role="form">
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-graduation-cap"></span>
												</span>	
												<textarea class="form-control" rows="3">Complete Academic Profile</textarea>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-suitcase"></span>
												</span>	
												<textarea class="form-control" rows="3">Complete Academic Experience</textarea>
											</div>											
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-book"></span>
												</span>	
												<select multiple class="form-control">
													<option>Physics</option>
													<option>Chemistry</option>
													<option>Maths</option>
													<option>Biology</option>
													<option>Geology</option>
												</select>
											</div>
											<div class="form-group">
												<label class="checkbox-inline">
													<input type="checkbox" checked></input>
													I'm willing to teach needy students for free! (charity)
												</label>
											</div>
										</form>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
						
						<div style="clear:both"></div>
						<center>
							<button type="submit" class="btn btn-default">Update</button>
						</center>
						<br>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
