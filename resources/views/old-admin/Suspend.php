<!DOCTYPE html>
<html lang="en">
<head>





    <title>Smart-Ed Admin Panel</title>
	<?php require_once('templatefiles/head.php'); ?>
	<style>

th {
	font: bold 11px "Trebuchet MS", Verdana, Arial, Helvetica,
	sans-serif;
	color: #6D929B;
	border-right: 1px solid #C1DAD7;
	border-bottom: 1px solid #C1DAD7;
	border-top: 1px solid #C1DAD7;
	letter-spacing: 2px;
	text-transform: uppercase;
	text-align: left;
	padding: 6px 6px 6px 12px;
	background: #CAE8EA url(images/bg_header.jpg) no-repeat;
}
td {
	border-right: 1px solid #C1DAD7;
	border-bottom: 1px solid #C1DAD7;
	background: #fff;
	padding: 6px 6px 6px 12px;
	color: #6D929B;
}



</style>
	
	
	
	
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		<?php require_once('template/header.php'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Account suspend</h1>
						
						
						 <table style="width:100%">
  <tr>
    <th>Student</th>
  </tr>
  <tr>
  <?php
  foreach ($users as $users){
  ?>
  <tr>
  <td>
  
  <form action="suspend" method="post" >
   <input type="text" name="id" value="<?php echo $users->id?>">
   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<div></div><button align="right" ><strong>Suspend</strong></button>
			 
		</form>
  </td>
  </tr>
  <?php
  }
  ?>
  </tr>
  
</table>

						
						
			
						<div class="col-md-3">
						
							<?php require_once('template/right-sidebar.php'); ?>
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	<?php require_once('template/footer.php'); ?>

</body>
</html>
