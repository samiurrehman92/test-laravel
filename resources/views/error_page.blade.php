<!DOCTYPE html>
<html lang="en">
	<head>
    <title>Smart-Ed | Learn Online Now!</title>
	
	<?php require_once('index_template/head.blade.php'); ?>
	<style>
		.features-box .features-content h4 {color:white;}
	</style>
  </head>

  <body>  
    <!-- Header Section Start -->
	<?php require_once('index_template/header.blade.php'); ?>
    <!-- Header Section End -->

	<div style="margin:10% 0px;">
		<h1 align="center">Ooops, there was an error!</h1>
		<p align="center">We're working to fix it, please retry agian later.</p>
	</div>

    <!-- Footer Section Start -->
    <footer>
	<?php require_once('index_template/footer.blade.php'); ?>
    </footer>
    <!-- Footer Section End -->      
  
</body></html>