<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome back, <?php echo Auth::user()->name ." (".Auth::user()->id.")"; ?></h1>
						@if(session()->has('message'))
							<div class="alert alert-success">
								{{ session()->get('message') }}
							</div>
						@endif
						<div class="row main-activity">
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-comments fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">{{count($dashboard['latest_convos'])}}</div>
												<div>Unread Messages!</div>
											</div>
										</div>
									</div>
									<a href="#">
										<div class="panel-footer">
											<span class="pull-left">View Messages</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-green">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-tasks fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">{{count($dashboard['orders'])}}</div>
												<div>Orders Requests!</div>
											</div>
										</div>
									</div>
									<a href="#">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-yellow">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-shopping-cart fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">{{count($dashboard['orders'])}}</div>
												<div>Pending Orders!</div>
											</div>
										</div>
									</div>
									<a href="#">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-red">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-money fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">{{$dashboard['credit']}}</div>
												<div>Credit Balance!</div>
											</div>
										</div>
									</div>
									<a href="/dashboard/view-transactions">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
						</div>						

						<div style="col-lg-8">
							<div class="row panel panel-default recent-activity">
							<div class="panel-heading">
								<i class="fa fa-clock-o fa-fw"></i> Recent Activity
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<ul class="timeline">
									<li>
										<div class="timeline-badge"><i class="fa fa-check"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Order #123 Marked Completed</h4>
												<p><small class="text-muted"><i class="fa fa-clock-o"></i> 11 hours </small>
												</p>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas suscipit facere rem dicta, debitis.</p>
											</div>
										</div>
									</li>
									<li class="timeline-inverted">
										<div class="timeline-badge warning"><i class="fa fa-credit-card"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">Payment Recieved for Order #321</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia repellendus.</p>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium maiores odit qui est tempora eos, nostrum provident explicabo dignissimos debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="timeline-badge danger"><i class="fa fa-bomb"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">You have a new order from teacher_qasim!</h4>
												<p><small class="text-muted"><i class="fa fa-bomb"></i> Order #321</small>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.</p>
											</div>
										</div>
									</li>
									<li class="timeline-inverted">
										<div class="timeline-badge info"><i class="fa fa-save"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">You have recieved a new Order Request</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis minus modi quam ipsum alias at est molestiae excepturi delectus nesciunt, quibusdam debitis amet, beatae consequuntur impedit nulla qui! Laborum, atque.</p>
												<hr>
												<div class="btn-group">
													<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
														<i class="fa fa-gear"></i> <span class="caret"></span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="create-order.php">Accept</a>
														</li>
														<li><a href="create-order.php">Create New Order</a>
														</li>
														<li><a href="chat-screen.php">Chat with Student</a>
														</li>
														<li class="divider"></li>
														<li><a href="#">Separated link</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
										</div>
										<div class="timeline-panel">
											<div class="timeline-heading">
												<h4 class="timeline-title">You updated your Education History</h4>
											</div>
											<div class="timeline-body">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<!-- /.panel-body -->
						</div>						
						</div>
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
