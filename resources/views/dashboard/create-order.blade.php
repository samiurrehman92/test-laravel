<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')
		
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Place the order</h1>
						
						<div class="col-md-9">
													
							{!! $orderrequest->profile_box !!}
							
							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-circle-o"></span>
								</span>	
								<form id="checkout_form" action="checkout" method="post">
							<?php $t = array(); preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $t); ?>
							<input type="hidden" name="orderid" value="<?= $t[0] ?>" >
								<select class="form-control" disabled>
									<option><?php echo $orderrequest->ordertype;?></option>
								</select>
							</div>
							</div>

							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-book"></span>
								</span>	
								<select class="form-control" disabled>
									<option><?php echo $orderrequest->course;?></option>
								</select>
							</div>						
							</div>
							
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								<textarea class="form-control" rows="3" disabled><?php echo $orderrequest->description?></textarea>
							</div>			

							<div class="form-group col-lg-6">
								<label>Requested Duration in Minutes</label>
								<p class="form-control-static" style="text-align:center"><?php echo $orderrequest->duration?></p>
							</div>
							
							<div class="col-lg-6">
							<label>Proposed Price</label>
							<div class="form-group input-group">
								<span class="input-group-addon">PKR</span>
								<input type="text" placeholder="<?php echo $orderrequest->price?>" class="form-control" value="" disabled>
								<span class="input-group-addon">.00</span>
							</div>
							</div>
							
							<div class="col-lg-6 col-lg-offset-4">
								<div class="form-group input-group">
									<select id="payment_type" name= "payment" class="form-control">
										<option value="" disabled>Select Payment Method</option>
										<option value="Smart-Ed Card">Smart-Ed Card</option>
										<option value="Credit/Debit Card">Credit/Debit Card</option>
										<!--<option value="Balance Transfer">Balance Transfer</option>
										<option value="Bank Transfer">Bank Transfer</option>-->
									</select>
								</div>
							</div>
							<div style="clear:both"></div>
							
							<br>
							<center>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<input type="hidden" name="stripe_token" id="stripe_token" value="">
								<button id="checkout_submit" type="submit" class="btn btn-default">Checkout</button>
							</center>
                           </form>
						</div>
			
						<div class="col-md-3">
						
							@include('dashboard.dashboard_template.right-sidebar')
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

<div id="stripePayment" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <form action="/charge" method="post" id="payment-form">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Credit or Debit Card Payment</h4>
      </div>
      <div class="modal-body">
		  <div class="form-row">
			<label for="card-element">
			  Please enter your card details carefully.
			</label>
			<div id="card-element">
			  <!-- a Stripe Element will be inserted here. -->
			</div>

			<!-- Used to display form errors -->
			<div id="card-errors" role="alert"></div>
		  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button class="btn btn-primary">Submit Payment</button>
      </div>
    </div>
	</form>						   

  </div>
</div>
	
<script src="https://js.stripe.com/v3/"></script>
<style>
.StripeElement {
  background-color: white;
  padding: 8px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script>
$(document).ready(function(){
	
	$( "#checkout_form" ).submit(function( e ) {
		if($("#payment_type").val()=="Credit/Debit Card" && $("#stripe_token").val()=="")
		{
			$('#stripePayment').modal('show');
			return false;
		}
	});
});
// Create a Stripe client
var stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '24px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
});
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
	$("#stripe_token").val(token.id);
	$( "#checkout_form" ).submit();
	$( "#checkout_submit" ).click();
}
</script>
	
</body>
</html>
