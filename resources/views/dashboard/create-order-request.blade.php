<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Create Order Request</h1>
						
						<div class="col-md-9">
						
					    <form action="store" method="post">
							<?php $t = array(); preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $t); ?>
							<input type="hidden" name="teacher" value="<?= $t[0] ?>" >
							{!! $teacher->profile_box !!}
							
							<div class="col-md-6">
							<div class="form-group input-group ">
								<span class="input-group-addon">
									<span class="fa fa-circle-o"></span>
								</span>	
								<select name="ordertype" class="form-control" required>
									<option value="">Order Type</option>
									<option value="Audio Call">Audio Call</option>
									<option value="Video Call">Video Call</option>
									<option value="Help Material">Help Material</option>
									<option value="Practice Material">Practice Material</option>
								</select>
							</div>
							</div>

							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-book"></span>
								</span>	
								<select name="course" class="form-control" required>
									<option value="">Course</option>
									<option value="Physics">Physics</option>
									<option value="Chemistry">Chemistry</option>
									<option value="Maths">Maths</option>
									<option value="English">English</option>
								</select>
							</div>						
							</div>
							
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								<textarea name="description"class="form-control" rows="3">Describe your order in complete detail</textarea>
							</div>			

							<div class="form-group col-lg-6">
								<label>Requested Duration in Minutes</label>
								<input  name= "duration" type="integer" class="form-control" required>
							</div>
							
							<div class="col-lg-6">
							<label>Proposed Price</label>
							<div class="form-group input-group">
								<span class="input-group-addon">PKR</span>
								<input name="price" type="text" placeholder="1500" class="form-control" required>
								<span class="input-group-addon">.00</span>
							</div>
							</div>
							
							<br><br>
							
							<center>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
								<input type="submit" name="submit" value="Send Offer" class="btn btn-default"></button>
							</center>
                          </form>
						</div>
			
						<div class="col-md-3">
						
							@include('dashboard.dashboard_template.right-sidebar')
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
