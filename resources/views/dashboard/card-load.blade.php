<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Load Smart-Ed Card</h1>
						
						<div class="col-md-9">
													
							
							
							
							
							<form action="load" method="post">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								
								<input name="number" type="integer" placeholder="Enter the nine digit Smart-Ed card number" class="form-control"></input>
							</div>			

							
							
							
							<br><br>
							<center>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
								<button type="submit" class="btn btn-default">Load</button>
							</center>
							</form>
						</div>
			
						<div class="col-md-3">
						
							@include('dashboard.dashboard_template.right-sidebar')
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')
</body>
</html>
