<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Order Completed</h1>
						
						<div class="col-md-9">
							
							{!! $orderrequest->profile_box !!}							

							<h3>Congratulations, your order has been completed!</h3>
							<br>
							<div class="col-md-6">
							
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="fa fa-check"></span>
									</span>	
									<form action="AcceptDelivery" method="post">
									<?php $t = array(); preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $t); ?>
							<input type="hidden" name="orderid" value="<?= $t[0] ?>" >
									<textarea name="feedback" class="form-control" rows="3">Please leave your feedback</textarea>
								</div>	
								<div class="form-group">
									<label>Rate the Teacher  </label>
									<input name="rating" type="integer" required>
								</div>								
								<center>
								<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
									<button type="submit" class="btn btn-default">Accept Delivery</button>
								</center>							
							</div>
							</form>
							<div class="col-md-6">
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="fa fa-times"></span>
									</span>	
									<form action="RequestRevision" method="post">
									<?php $t = array(); preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $t); ?>
							<input type="hidden" name="orderid" value="<?= $t[0] ?>" >
									<textarea name="revision" class="form-control" rows="3">Describe what was wrong with the delivery</textarea>
								</div>			
								<center>
								<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
									<button type="submit" class="btn btn-default">Request Revision</button>
								</center>
								</form>
							</div>
						
						</div>
			
						<div class="col-md-3">
						
							@include('dashboard.dashboard_template.right-sidebar')
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
