<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Respond to Order Request</h1>
						
						<div class="col-md-9">
													
							{!! $orderrequest->profile_box !!}
							
							<div class="col-md-6">
							<div class="form-group input-group ">
								<span class="input-group-addon">
									<span class="fa fa-circle-o"></span>
								</span>	
								<form action="Respond" method="post">
								<?php $t = array(); preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $t); ?>
							<input type="hidden" name="orderid" value="<?= $t[0] ?>" >
								<select name="ordertype" class="form-control">

									<option value="Audio Call" <?php if($orderrequest->ordertype=="Audio Call"){echo "selected";}?> >Audio Call</option>
									<option value="Video Call"<?php if($orderrequest->ordertype=="Video Call"){echo "selected";}?> >Video Call</option>
									<option value="Help Material"<?php if($orderrequest->ordertype=="Help Material"){echo "selected";}?> >Help Material</option>
									<option value="Practice Material" <?php if($orderrequest->ordertype=="Practice Material"){echo "selected";}?>>Practice Material</option>
								</select>
							</div>
							</div>

							<div class="col-md-6">
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-book"></span>
								</span>	
								<select name="course" class="form-control">
									
									<option value="Physics" <?php if($orderrequest->course=="Physics"){echo "selected";}?>  >Physics</option>
									<option value="Chemistry" <?php if($orderrequest->course=="Chemistry"){echo "selected";}?>  >Chemistry</option>
									<option value="Maths" <?php if($orderrequest->course=="Maths"){echo "selected";}?>  >Maths</option>
									<option value="English" <?php if($orderrequest->course=="English"){echo "selected";}?>  >English</option>
								</select>
							</div>						
							</div>
							
							<div class="form-group input-group">
								<span class="input-group-addon">
									<span class="fa fa-reorder"></span>
								</span>	
								<textarea name="description"class="form-control" rows="3"><?php echo $orderrequest->description?></textarea>
							</div>			

							<div class="form-group col-lg-6">
								<label>Requested Duration in Minutes</label>
								<input  name= "duration" type="integer" value="<?php echo $orderrequest->duration?>" class="form-control">
							</div>
							
							<div class="col-lg-6">
							<label>Proposed Price</label>
							<div class="form-group input-group">
								<span class="input-group-addon">PKR</span>
								<input name="price" type="text" value="<?php echo $orderrequest->price?>" class="form-control">
								<span class="input-group-addon">.00</span>
							</div>
							</div>
							
							<br><br>
							
							<center>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
								<input type="submit" name="Accept" value="Accept" class="btn btn-default"></button>
								<input type="submit" name="Reject" value="Reject" class="btn btn-default"></button>
							</center>
                          </form>
						</div>
			
						<div class="col-md-3">
						
							@include('dashboard.dashboard_template.right-sidebar')
						
						</div>
						
					</div>
 
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
		@include('dashboard.dashboard_template.footer')

</body>
</html>
