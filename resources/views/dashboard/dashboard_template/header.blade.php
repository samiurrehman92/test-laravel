<nav class="navbar navbar-default main-navigation" role="navigation">
  <div class="navbar-header">
	<a class="navbar-brand logo" href="/dashboard" style="width:auto;">
		<img src="/images/logo.png" alt="Smart-Ed"> <h2>Smart-Ed</h2>
	</a>
  </div>

	<div class="container">
	  <!-- Navbar Start -->
		 <ul class="nav navbar-top-links navbar-right">
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-messages">
				@if(!empty($dashboard['latest_convos']))
					@foreach($dashboard['latest_convos'] as $convo)
					<li>
							<a href="/dashboard/chat/{{$convo->user_id}}">
								<div>
									<strong>{{$convo->user_name}}</strong>
									<span class="pull-right text-muted">
										<em class="timestamp" timestamp="{{$convo->timestamp}}">Just Now</em>
									</span>
								</div>
								<div>{{$convo->Message_Text}}</div>
							</a>
						</li>
						<li class="divider"></li>
					@endforeach
				@endif
						<!--<a class="text-center" href="#">
							<strong>Read All Messages</strong>
							<i class="fa fa-angle-right"></i>
						</a>-->
					</li>
				</ul>
				<!-- /.dropdown-messages -->
			</li>
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-tasks">
				@if(!empty($dashboard['orders']) && count($dashboard['orders'])>0)
					@foreach($dashboard['orders'] as $order)
					<li>
						<a href="{{$order->url}}">
							<div>
								<p>
									<strong>{{$order->title}}</strong>
									<span class="pull-right text-muted">{{$order->status}}</span>
								</p>
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 5%">
										<span class="sr-only">40% Complete (success)</span>
									</div>
								</div>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					@endforeach
				@else
					<li class="divider"></li>
					<li>
						<p class="text-center">
							You have no pending orders/requests.
						</p>
					</li>
					<li class="divider"></li>
				@endif
				</ul>
				<!-- /.dropdown-tasks -->
			</li>
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-alerts">
					<li>
						<a href="#">
							<div>
								<i class="fa fa-comment fa-fw"></i> New Comment
								<span class="pull-right text-muted small">4 minutes ago</span>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">
							<div>
								<i class="fa fa-twitter fa-fw"></i> 3 New Followers
								<span class="pull-right text-muted small">12 minutes ago</span>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">
							<div>
								<i class="fa fa-envelope fa-fw"></i> Message Sent
								<span class="pull-right text-muted small">4 minutes ago</span>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">
							<div>
								<i class="fa fa-tasks fa-fw"></i> New Task
								<span class="pull-right text-muted small">4 minutes ago</span>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">
							<div>
								<i class="fa fa-upload fa-fw"></i> Server Rebooted
								<span class="pull-right text-muted small">4 minutes ago</span>
							</div>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="text-center" href="#">
							<strong>See All Alerts</strong>
							<i class="fa fa-angle-right"></i>
						</a>
					</li>
				</ul>
				<!-- /.dropdown-alerts -->
			</li>
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="/dashboard/edit-profile"><i class="fa fa-user fa-fw"></i>Edit Profile</a>
					</li>
					@if($dashboard['is_admin']==0)
					<li><a href="/dashboard/customer-support"><i class="fa fa-support fa-fw"></i> Customer Support</a>
					</li>
					@endif
					<li><a href="/dashboard/user-settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					<li><a href="/dashboard/card-load"><i class="fa fa-money fa-fw"></i> Load Card</a>
					</li>
					<li class="divider"></li>
					<li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
					</li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>
	 <!-- Navbar End -->
	</div>

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li class="sidebar-search">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
					<!-- /input-group -->
				</li>
				<li>
					<a href="/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
				</li>
				<li>
					<a href="/dashboard/search-teachers"><i class="fa fa-check-square-o fa-fw"></i> Search Teachers</a>
				</li>
				<li>
					<a href="/dashboard/view-transactions"><i class="fa fa-check-square-o fa-fw"></i> View Transactions</a>
				</li>
				@if($dashboard['user']['teacher']==1)
				<li>
					<a href="/dashboard/all-files"><i class="fa fa-check-square-o fa-fw"></i> Files Directory</a>
				</li>
				@endif
				@if($dashboard['user']['teacher']==0)
				<li>
					<a href="/dashboard/myfiles"><i class="fa fa-check-square-o fa-fw"></i>My Files</a>
				</li>
				@endif
				@if($dashboard['is_admin']==1)
				<li>
					<a href="/dashboard/all-users"><i class="fa fa-check-square-o fa-fw"></i>Users Directory</a>
				</li>
				<li>
					<a href="/dashboard/all-orders"><i class="fa fa-check-square-o fa-fw"></i>Orders Directory</a>
				</li>
				<li>
					<a href="/dashboard/add-cards"><i class="fa fa-check-square-o fa-fw"></i>Add Smart-cards</a>
				</li>
				@endif
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>