    <!-- jQuery -->
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
	<script src="/dashboard_files/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/dashboard_files/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/dashboard_files/vendor/metisMenu/metisMenu.min.js"></script>

	<!--Slider Input Bar-->
    <script src="/dashboard_files/vendor/slider/js/bootstrap-slider.js"></script>	
	
    <!-- Custom Theme JavaScript -->
    <script src="/dashboard_files/dist/js/sb-admin-2.js"></script>

	<script>
		$(document).ready(function() {
			$('#ex1,#ex2,#ex3,#ex4,#ex5').slider({
				formatter: function(value) {
					return 'Current value: ' + value;
				}
			});
			$(".dropdown-messages").find(".timestamp").each(function(){
				$(this).text(timeDifference($(this).attr("timestamp")));
			});

		});	
		
		function timeDifference(timestamp) {

			var t = timestamp.split(/[- :]/);
			var previous = new Date(timestamp);

			var msPerMinute = 60 * 1000;
			var msPerHour = msPerMinute * 60;
			var msPerDay = msPerHour * 24;
			var msPerMonth = msPerDay * 30;
			var msPerYear = msPerDay * 365;

			var elapsed = new Date() - previous;
			
			if (elapsed < msPerMinute) {
				 return 'few seconds ago';   
			}

			else if (elapsed < msPerHour) {
				 return Math.round(elapsed/msPerMinute) + ' minutes ago';   
			}

			else if (elapsed < msPerDay ) {
				 return Math.round(elapsed/msPerHour ) + ' hours ago';   
			}

			else if (elapsed < msPerMonth) {
				return Math.round(elapsed/msPerDay) + ' days ago';   
			}

			else if (elapsed < msPerYear) {
				return Math.round(elapsed/msPerMonth) + ' months ago';   
			}

			else {
				return Math.round(elapsed/msPerYear ) + ' years ago';   
			}
		}
		
	</script>