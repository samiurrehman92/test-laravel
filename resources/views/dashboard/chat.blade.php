<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<br>
						<br>
						<div class="chat-panel panel panel-default">
                        <div class="panel-heading">
                            <h2 style="display:inline;"><i class="fa fa-comments fa-fw"></i>Chat with <?php echo $user2->name; ?></h2>
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                                <ul class="dropdown-menu slidedown">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-refresh fa-fw"></i> Refresh
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-check-circle fa-fw"></i> Available
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-times fa-fw"></i> Busy
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-clock-o fa-fw"></i> Away
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-sign-out fa-fw"></i> Sign Out
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="chat">
                            </ul>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here...">
                                <span class="input-group-btn">
                                    <button class="btn btn-warning btn-sm" id="btn-chat">
                                        Send
                                    </button>
                                </span>
                            </div>
                        </div>
                        <!-- /.panel-footer -->
                    </div>						
					
					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

<audio src="/sounds/tada.wav" autostart="false" width="0" height="0" id="msg_audio" />
<script>
var lastTimeID = 0;

$(document).ready(function() {
  $('#btn-chat').click( function() {
    sendChatText();
    $('#btn-input').val("");
  });
  
	$('#btn-input').keypress(function (e) {
	   if(e.which ==13)
			$('#btn-chat').click();
	});  
  startChat();
});

function startChat(){
  setInterval( function() { getChatText(); }, 2000);
  setInterval( function() { updateTime(); }, 60000);
}

function updateTime()
{
	$(".chat").find(".timestamp").each(function(){
		$(this).text(timeDifference($(this).attr("timestamp")));
	});
	
}

function getChatText() {
	var sound = document.getElementById("msg_audio");
	sound.volume = 1;
  $.ajax({
    type: "GET",
    url: "/chat_get?user2="+<?= $user2->id ?>+"&lastTimeID=" + lastTimeID,
	success: function(response){ // What to do if we succeed
		$('.chat').append(response);
		if(response)
		{
			lastTimeID = $('.chat li:last').attr('id').substring(4);
			sound.currentTime = 0;
			sound.play();
			updateTime();
			$('.chat').scrollTop($('.chat')[0].scrollHeight);

		}
	},
		error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
		console.log(JSON.stringify(jqXHR));
			console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		}
  });
}

function sendChatText(){
  var chatInput = $('#btn-input').val();
  
  if(checkAllowed(chatInput))
	  alert('You are not allowed to share personal information');
  else if(chatInput != ""){
    $.ajax({
      type: "POST",
      url: "/chat_send",
	  data: { sender_id: <?= Auth::user()->id ?>, reciever_id: <?= $user2->id ?>, msg: chatInput, _token: "<?php echo csrf_token(); ?>" },
		success: function(response){ // What to do if we succeed
			console.log(response);
		},
		error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
		console.log(JSON.stringify(jqXHR));
			console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		}	  
    });
  }
}

function timeDifference(timestamp) {

	var t = timestamp.split(/[- :]/);
	var previous = new Date(timestamp);

    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = new Date() - previous;
	
    if (elapsed < msPerMinute) {
         return 'few seconds ago';   
    }

    else if (elapsed < msPerHour) {
         return Math.round(elapsed/msPerMinute) + ' minutes ago';   
    }

    else if (elapsed < msPerDay ) {
         return Math.round(elapsed/msPerHour ) + ' hours ago';   
    }

    else if (elapsed < msPerMonth) {
        return Math.round(elapsed/msPerDay) + ' days ago';   
    }

    else if (elapsed < msPerYear) {
        return Math.round(elapsed/msPerMonth) + ' months ago';   
    }

    else {
        return Math.round(elapsed/msPerYear ) + ' years ago';   
    }
}

function checkAllowed(text) {
	//email
    var email = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
	var phone = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img;

    if(email.test(text))
		return true;
    else if(phone.test(text))
		return true;
}

function checkIfEmailInString(text) { 

}
</script>
</body>
</html>
