<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Orders Directory</h1>
						@if(session()->has('message'))
							<div class="alert alert-success">
								{{ session()->get('message') }}
							</div>
						@endif
						
						<table class="table table-striped">
							<tr>
								<th>Order ID</th>
								<th>Student</th>
								<th>Teacher</th>
								<th>Order Type</th>
								<th>Order Status </th>
								<th>Actions</th>
							</tr>
						@foreach($orderrequests as $orderrequest)
							<tr >
								<td>{{$orderrequest->id}}</td>
								<td>{{$orderrequest->student_name}}</td>
								<td>{{$orderrequest->teacher_name}}</td>
								<td>{{$orderrequest->ordertype}}</td>
								<td> {{$orderrequest->status}}</td>
								
								<td>
									<a href="/dashboard/suspend_order/{{$orderrequest->id}}"><button>Suspend</button></a>
									<a href="/dashboard/activate_order/{{$orderrequest->id}}"><button>Activate</button></a>
								</td>
							</tr>
						@endforeach
						</table>
						
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
