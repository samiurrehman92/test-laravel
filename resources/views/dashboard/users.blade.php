<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Users Directory</h1>
						@if(session()->has('message'))
							<div class="alert alert-success">
								{{ session()->get('message') }}
							</div>
						@endif
						
						<table class="table table-striped">
							<tr>
								<th>User ID</th>
								<th>Full Name</th>
								<th>Account Status</th>
								<th>Account Type</th>
								<th>Member Type</th>
								<th>Actions</th>
							</tr>
						@foreach($users as $user)
							<tr <?php if($user->status=="Suspended") echo 'style="background:gray"'; ?>>
								<td>{{$user->id}}</td>
								<td>{{$user->name}}</td>
								<td>{{$user->status}}</td>
								<td><?php if ($user->teacher) echo "Teacher"; else echo "Student"; ?></td>
								<td>{{$user->created_at}}</td>
								<td>
									<a href="/dashboard/activate_user/{{$user->id}}"><button>Active</button></a>
									<a href="/dashboard/suspend_user/{{$user->id}}"><button>Deactive</button></a>
									<a href="/dashboard/delete_user/{{$user->id}}"><button>Delete</button></a>
								</td>
							</tr>
						@endforeach
						</table>
						
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
