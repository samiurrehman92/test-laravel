<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Documents Upload</h1>
					
						<div class="panel panel-default col-lg-5" style="padding:0px;">
							<div class="panel-heading">
								Upload Documents
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<center>
											<div class="form-group">
												<label>Upload Document</label>


												<form method="POST" enctype="multipart/form-data">
													{{csrf_field()}}
													{{method_field('POST')}}

													<input type="file" name="doc" required>
													<br />
													<br /><br /><br /><br /><br /><br />
													

												
											</div>
										</center>
										
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <div class="panel panel-default col-lg-5 col-lg-offset-1" style="padding:0px;">
							<div class="panel-heading">
								Required Fields
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<center>
											<div class="form-group">
												<label>Required Fields</label>


												
													

													
													<br />
													<input type="text" name="title" placeholder="Title" required >
													<br />
													<br />
													Course
													<select name="course" class="form-control" required>
														<option value="" disabled>Subject Related to</option>
														<option value="Physics">Physics</option>
														<option value="Chemistry">Chemistry</option>
														<option value="Maths">Maths</option>
														<option value="English">English</option>
													</select>
													
													<br />
													<br />
													Type of Practice Material
													<select name="type" class="form-control" required>
														<option value="0">Notes</option>
														<option value="1">Help material</option>
														<option value="2">Practice Material</option>
														
													</select>
													<br />
													<br />
													Privacy
													<select name="privacy" class="form-control" required>
														<option value="2">Public</option>
														<option value="1"> Specific Users</option>
														<option value="0">Only Me</option>
												
													</select>

													

												
											</div>
										</center>
										
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>


            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<center> <button type="submit" align="center" >Upload Doc</button>
</center>
</form>
    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
