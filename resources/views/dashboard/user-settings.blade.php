<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User Settings</h1>
						
						<div class="panel panel-default col-lg-6" style="padding:0px;">
							<div class="panel-heading">
								Change Password
							</div>
							<div class="panel-body">
								<form method="POST" action="/dashboard/update_password" id="update_password_form">
								<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" name="curr_pass" type="password" placeholder="Current Password" required>
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" id="new_pass" name="new_pass" type="password" placeholder="New Password" required>
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-modal-window"></span>
									</span>	
									<input class="form-control" id="new_pass_confirmed" name="new_pass_confirmed" type="password" placeholder="Confirm New Password">
								</div>
								<center>
									<button type="submit" class="btn btn-default">Update</button>
								</center>
								</form>
							</div>							
						</div>
						<div class="panel panel-default col-lg-5 col-lg-offset-1" style="padding:0px;">
							<div class="panel-heading">
								Deactivate Account
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="checkbox-inline">
										<input type="checkbox" checked></input>
										I understand that my information will be lost.
									</label>
								</div>
								<center>
									<button type="submit" onClick="location.href = '/dashboard/suspend_user/<?= $dashboard['user']['id']; ?>';" class="btn btn-default">Deactivate</button>
								</center>
							</div>
						</div>
                   <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

<script>
$(document).ready(function(){
	
	function checkPasswordMatch() {
    var password = $("#new_pass").val();
    var confirmPassword = $("#new_pass_confirmed").val();

    if (password != confirmPassword)
        $("#new_pass_confirmed").parent().addClass(" has-error");
	else
        $("#new_pass_confirmed").parent().removeClass(" has-error");		
	}
	
	$("#new_pass_confirmed").keyup(checkPasswordMatch);
	
	$( "#update_password_form" ).submit(function( event ) {
		var password = $("#new_pass").val();
		var confirmPassword = $("#new_pass_confirmed").val();

		if (password != confirmPassword)
		{		
		  alert( "The new password fields don't match" );
		  event.preventDefault();
		}
	});
});
</script>
</body>
</html>
