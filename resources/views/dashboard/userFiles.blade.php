<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
    @include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
        @include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                            <button style=" margin-top:25px;float:right	" onclick="location.href = '/dashboard/uploadDocs';" class="btn btn-default">Upload Files</button>

                    
                        <h1 class="page-header">Documents Uploaded</h1>

                        <table class="table table-striped">
                            <tr>
                                <th>File Title</th>
                                <th>Material Type</th>
                                <th>Subject</th>
                                <th>Privacy</th>
                                <th>Actions</th>
                            </tr>

                            @for ($i = 0; $i < count($files['name']); $i++)
                            <tr>          
                            <td>{{$files['title'][$i] }}</td>
                            <td>{{$files['type'][$i]}}</td>
                            <td>{{$files['subject'][$i] }}</td>
                            <td>{{$files['privacy'][$i] }}</td>
                            <td>
                                <a href="{{$files['url'][$i]}}"  target="_blank"><button>Open</button></a>
                                <a href="/dashboard/Docs/nomore/{{ $files['id'][$i]}}"><button>Delete</button></a>
                            </td>                    
                            </tr>

                            @endfor
                            </table>
                            





                            </div>
                            </div>
                            </div>
              

            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- /#footer -->
    @include('dashboard.dashboard_template.footer')

</body>
</html>
