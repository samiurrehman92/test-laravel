<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')

    <!-- use http://una.im/CSSgram/ for filters -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/una/CSSgram/master/source/css/cssgram.css">
    <!-- app styles -->
    <link rel="stylesheet" href="/video-call/styles.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />	
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')
		
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Order {{$order->id}}</h1>

                       <form method="POST" action="{{url('/materialorder')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    {{method_field('POST')}}

					<input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                        <input type="hidden" name="order_id" id="order_id" value="{{$order->id}}">


						<a href="/dashboard/customer-support/">
							<button>Create Dispute</button>
						</a>
					</div>
 

                 <table class="table table-striped" >
                            <tr>
                                <th>File Title</th>
                                <th>Material Type</th>
                                <th>Subject</th>
                                <th>Privacy</th>
                                <th>Select</th>
                            </tr>

                            @for ($i = 0; $i < count($files['name']); $i++)
                            <tr>          
                            <td>{{$files['title'][$i] }}</td>
                            <td>{{$files['type'][$i]}}</td>
                            <td>{{$files['subject'][$i] }}</td>
                            <td>{{$files['privacy'][$i] }}</td>
                            <td>
                                <input type="checkbox" name="fileids" value="{{$files['id'][$i]}}" />
                            </td>                    
                            </tr>
                            @endfor
                            </table>
                <!-- /.row -->
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

  
</body>
</html>
