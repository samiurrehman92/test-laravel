<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
		
		
		
		
		
									<div class="col-lg-3 col-md-6" style=" float:right; margin-top:25px;">
								<div class="panel panel-red">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-money fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">{{$dashboard['credit']}}</div>
												<div>Credit Balance!</div>
											</div>
										</div>
									</div>
									<a href="view-transactions/withdrawal">
										<div class="panel-footer">
											<span class="pull-left">Withdrawal</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
		
		
		
		
		
		
		
		
            <div class="container-fluid">
			
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">View Transactions</h1>
						@if(session()->has('message'))
							<div class="alert alert-success">
								{{ session()->get('message') }}
							</div>
						@endif
						
						<table class="table table-striped">
							<tr>
								<th>Date Time</th>
								<th>Description</th>
								<th style="text-align:right">Amount</th>
								
							</tr>
						@foreach($transactions as $transaction)
							<tr >
								<td><?php echo date("d-M-Y g:i a",strtotime($transaction->date_time)) ?></td>
								<td>{{$transaction->detail}}</td>
								<td align="right" <?php  if($transaction->amount<0)
								{
									echo 'style="color: red"';
									}
									?>>
									<?= number_format($transaction->amount)?>
						
								</td>
							</tr>
						@endforeach
						</table>
						
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
