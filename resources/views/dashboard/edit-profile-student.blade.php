<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Student Profile</h1>
					
						<div class="panel panel-default col-lg-5" style="padding:0px;">
							<div class="panel-heading">
								Edit Basic Profile
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<center>
											<img src="{{$data['url']}}" width="100%"></img>
											<div class="form-group">
												<label>Change Profile Image</label>


												<form method="POST" action="{{url('/postPic')}}" enctype="multipart/form-data">
													{{csrf_field()}}
													{{method_field('POST')}}

													<input type="file" name="dp" required>
													<br />

													<button type="submit" >Upload Pic</button>

												</form>
											</div>
										</center>
										<form role="form"  method="POST" action="{{url('/updateuser')}}" >

										{{csrf_field()}}
													{{method_field('POST')}}
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-user"></span>
												</span>	
												<input class="form-control" placeholder="Full Name" 
												value=" {{ $data['name']  }} "  name="name" id="name">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-envelope-o"></span>
												</span>	
												<input class="form-control" type="email" placeholder="Email Address"
												value=" {{ $data['email']  }} "  name="email" id="email" readonly>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-phone"></span>
												</span>	
												<input value = "{{ $data['contact']  }}"class="form-control" type="phone" placeholder="Contact Number (e.g. +92-321-3213213)" name="contact" id="contact">
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-home"></span>
												</span>	
												<textarea class="form-control" rows="3" name="address" id="address">{{$data['address']}}</textarea>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-building-o"></span>
												</span>	
												<select name="city" class="form-control">
													<option name="">City</option>
													<option value="Lahore" <?php if($data['city']=="Lahore") echo "selected"; ?>>Lahore</option>
													<option value="Islamabad" <?php if($data['city']=="Islamabad") echo "selected"; ?>>Islamabad</option>
													<option value="Peshawar" <?php if($data['city']=="Peshawar") echo "selected"; ?>>Peshawar</option>
													<option value="Karachi" <?php if($data['city']=="Karachi") echo "selected"; ?>>Karachi</option>
												</select>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-bank"></span>
												</span>	
												<select name="country" class="form-control">
													<option name="">Country</option>
													<option value="Pakistan"<?php if($data['country']=="Pakistan") echo "selected"; ?>>Pakistan</option>
													<option value="Iran" <?php if($data['country']=="Iran") echo "selected"; ?>>Iran</option>
													<option value="Afghanistan" <?php if($data['country']=="Afghanistan") echo "selected"; ?>>Afghanistan</option>
													<option value="India" <?php if($data['country']=="India") echo "selected"; ?>>India</option>
												</select>
											</div>
										
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
						
						<div class="panel panel-default col-lg-6 col-lg-offset-1" style="padding:0px;">
							<div class="panel-heading">
								Edit Educational Profile
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-graduation-cap"></span>
												</span>	
												<textarea id="edu" class="form-control" rows="3" name="education">{{$data['education']}}</textarea>
											</div>
									</div>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
						
						<div style="clear:both"></div>
						<center>
							<button type="submit" class="btn btn-default">Update</button>
						</center>
						<br>
						</form>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
