<!DOCTYPE html>
<html lang="en">

<head>
    <title>Smart-Ed Admin Panel</title>
	@include('dashboard.dashboard_template.head')
</head>

<body class="admin">

    <div id="wrapper" class="page">
        <!-- Navigation -->
		@include('dashboard.dashboard_template.header')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Search Teacher</h1>

						<div class="panel panel-default col-lg-3" style="padding:0px;">
							<div class="panel-heading">
								Filter Results
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form method="POST" role="form" action="" >
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-user"></span>
												</span>	
												<input class="form-control" name="name" value="<?php if(isset($data['name'])){echo $data['name'];} ?>" placeholder="Teacher's Name">
											</div>
											<div class="form-group">
												<label>Education Type</label>
												<div class="checkbox">
													<label>
														<input name="Olevel" type="checkbox" value="1" <?php if(isset($data['olevel'])){if($data['olevel']) echo "checked";} ?>>O' Level
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input name="Matric"  type="checkbox" value="1"<?php if(isset($data['matric'])){if($data['matric']) echo "checked";} ?>>Matric
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input name="Alevel" type="checkbox" value="1"<?php if(isset($data['alevel'])){if($data['alevel']) echo "checked";} ?>>A' Level
													</label>
												</div>
											</div>		

											<div class="form-group">
												<label>Experience in years</label>
												<input name="experience" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="2"/>
											</div>

											<div class="form-group">
												<label>Rating in stars</label>
												<input name="rating" name="rating" id="ex2" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="5" data-slider-step="1" data-slider-value="2"/>
											</div>

											
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-building-o"></span>
												</span>	
												<select name="city" class="form-control">
													<option value="0" >City</option>
													<option value="Lahore" <?php if(isset($data['city'])){if($data['city']=="Lahore") echo "selected";} ?>>Lahore</option>
													<option value="Islamabad" <?php if(isset($data['city'])){if($data['city']=="Islamabad") echo "selected";} ?>>Islamabad</option>
													<option value="Peshawar" <?php if(isset($data['city'])){if($data['city']=="Peshawar") echo "selected";} ?>>Peshawar</option>
													<option value="Karachi" <?php if(isset($data['city'])){if($data['city']=="Karachi") echo "selected";} ?>>Karachi</option>
												</select>
											</div>
											<div class="form-group input-group">
												<span class="input-group-addon">
													<span class="fa fa-bank"></span>
												</span>	
												<select name="country" class="form-control">
													<option value="0">Country</option>
													<option value="Pakistan" <?php if(isset($data['country'])){if($data['country']=="Pakistan") echo "selected";} ?>>Pakistan</option>
													<option value="Iran" <?php if(isset($data['country'])){if($data['country']=="Iran") echo "selected";} ?>>Iran</option>
													<option value="Afghanistan" <?php if(isset($data['country'])){if($data['country']=="Afghanistan") echo "selected";} ?>>Afghanistan</option>
													<option value="India" <?php if(isset($data['country'])){if($data['country']=="India") echo "selected";} ?>>India</option>
												</select>
											</div>
											<div class="form-group input-group">
												<div class="checkbox">
													<label>
														<input name="charity" type="checkbox" value="1" <?php if(isset($data['charity'])){if($data['charity']) echo "checked";} ?>>Teaching for Charity
													</label>
												</div>
											</div>
											<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
											<button type="submit" class="btn btn-default">Submit</button>
										</form>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>
						
						<div class="panel panel-default search-results col-lg-8" style="margin-left:15px; padding:0px;">
							<div class="panel-heading">
								Search Results
							</div>
							<div class="panel-body">
								<?php /*
								<div class="row results-header">
									<div class="col-sm-6">
										<div class="dataTables_length" id="dataTables-example_length">
										<select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="0">Entries per page</option>
												<option value="10">10</option>
												<option value="25">25</option>
												<option value="50">50</option>
												<option value="100">100</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="0">Default Search</option>
												<option value="10">Sort by Price</option>
												<option value="25">Sort by Experience</option>
												<option value="50">Sort by Rating</option>
											</select>							
									</div>
								</div>	
									*/
								?>
								
								<div class="row results-body">
								@foreach($users as $user)
									<div class="search-result-item">
										<div class="col-md-2">
											<img src="/images/user.png"></img>
										</div>
										<div class="col-md-9">
											<a href="/profile/{{ $user->id }}"><h6>Teacher {{ $user->name }}</h6></a>
											<span>O Level Physics Teacher at Beaconhouse School System</span>
											<span>Physics Teacher for 5 years</span>
											<span>Rating 3 stars</span>
										</div>
										<div class="col-md-1 action-buttons">
											<a href="/dashboard/chat/{{ $user->id }}"><p class="fa fa-comments"></p></a>
											<a href="#"><span class="glyphicon glyphicon-star"></span></a>
											<a href="/dashboard/create-order-request/{{ $user->id }}"><p class="fa fa-rocket"></p></a>
										</div>
									</div>
								@endforeach

								</div>
								
								<div class="row results-footer">
									<div class="col-sm-6">
										<div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 1 to <?= count($users) ?> of <?= count($users) ?> entries</div>
									</div>
									<div class="col-sm-6">
										<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Previous</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">2</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4</a><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Next</a></li></ul>
										</div>
									</div>
								</div>
							
							</div>
						</div>						

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- /#footer -->
	@include('dashboard.dashboard_template.footer')

</body>
</html>
